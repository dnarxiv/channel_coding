<p align="center"><img src="utility/imgReadme/0001.jpg" alt="p1" width="100%"></p>

----
----
```sh
> ./channel_encoder.sh -i refSeq_path -p primers_path -o output_path -G matrix_path
```	
<p align="center"><img src="utility/imgReadme/0002.jpg" alt="p2" width="100%"></p>

----
----
```sh
> ./channel.sh -i refSeq_path -n nbr_reads -o output_path -k 6
```	
<p align="center"><img src="utility/imgReadme/0003.jpg" alt="p3" width="100%"></p>

----
----
```sh
> ./consensus.sh -l expected_len -i fastq_path -p primers_path -o output_path -m 100
```	
<p align="center"><img src="utility/imgReadme/0004.jpg" alt="p4" width="100%"></p>

----
----
```sh
> ./channel_decoder.sh -i seq_path -l expected_len -o output_path -H matrix_path
```	
<p align="center"><img src="utility/imgReadme/0005.jpg" alt="p5" width="100%"></p>

----
----
```sh
> ./decoding_scheme.sh -l expected_len i fastq_path -p primers_path -o output_path -avoidLongHP 0 -m 100 -H matrix_path
```	
<p align="center"><img src="utility/imgReadme/0006.jpg" alt="p6" width="100%"></p>
<p align="center"><img src="utility/imgReadme/0007.jpg" alt="p7" width="100%"></p>

----
----
```sh
> ./noLongHP3 1 CGTACATGAC
```	
```sh
> ./noLongHP3 2 CGTAACCTACA
```	
<p align="center"><img src="utility/imgReadme/0008.jpg" alt="p8" width="100%"></p>



----
----
```sh
> julia Concat_CRC_CC_Enc.jl in_Seq CRC_bool
```	
<p align="center"><img src="utility/imgReadme/0009_Enc_CC_CRC.jpg" alt="p6" width="100%"></p>


----
----
```sh
> julia --thread auto Concat_CRC_CC_Dec.jl in_Seqs Nbr_Seqs Expected_Len CRC_bool
```	
<p align="center"><img src="utility/imgReadme/0010_Dec_CC_CRC.jpg" alt="p6" width="100%"></p>
