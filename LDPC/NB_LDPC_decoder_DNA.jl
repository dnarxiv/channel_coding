#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

###################
#    Functions    #
###################

	#**connect v-nodes to c-nodes (each c-node will have indexes of all its v-nodes)**#
	function VToCnodes(H,cnodes)

		n=length(H[:,1]) #number of line or equations or c-nodes
	
		for i=1:n
			push!(cnodes,[]) #new equation or connexion 
			m=length(H[i,:])
			for j=1:m
				if(H[i,j]!=0) #so the v-nodes is include on the parity check equation
					push!(cnodes[i],j) 
				end
			end
		end	
	end



	#check if code-word valid
	function valid2(y,H,cnodes)
		
		n=length(cnodes)
		for i=1:n

			m=length(cnodes[i])

			tmp=0 
			for j=1:m 
				tmpM=multGF(y[cnodes[i][j]],H[i,cnodes[i][j]]) #
				tmp=addGF(tmp,tmpM); #	
			end
			if(tmp!=0)
				#code-word not valid
				return false
			end
		end

		return true

	end

	
	
	function Dna2Dec(y)

		len=length(y)
		dec=[]
		for i=1:len
			if(y[i]=='A')
				push!(dec,0);
			elseif(y[i]=='C')
				push!(dec,1);
			elseif(y[i]=='G')
				push!(dec,2);
			elseif(y[i]=='T')
				push!(dec,3);
			end					
		end

		return dec
	end




	#---
	#---  GF(4) Arithmetic 
	#---

	include("GF4Arithmetic.jl")


####################
#	Init()	   #
####################
	using MAT
	using DelimitedFiles

	
	
	cnodes=[]
	vnodes=[]
	
	if(length(ARGS)<2)
		println("error: NB-LDPC Decoder, no enough arguments!")
		exit(1)
	end

	H=[] #parity check matrix


	matName=ARGS[2]

	ext=split(matName,'.')[end]

	if(ext=="mat" || ext=="m")
		file = matopen(matName)
		H = Int.(read(file, "H"))
	else #assume a text file
		H = readdlm(matName)
		H = Int.(H) 	
	end

	
	m=length(H[1,:]) #columns
	n=length(H[:,1]) #rows

####################
#	Main()	   #
####################

	
	VToCnodes(H,cnodes) #init connexions v-nodes <-> cnodes





	fasta=readdlm(ARGS[1])
	r=""
	if(fasta[1,1]==">")
		r=fasta[2]
	else
		r=fasta[1]
	end

	rLen=length(r)

	if(rLen>=1)
		if(r[1]!='A' && r[1]!='C' && r[1]!='G' && r[1]!='T')
			println("error: unknown bases=",r[1]," detected! please put a right fasta file")
			exit(1)
		end
	else
		println("error: please put a right fasta file")
		exit(1)
	end


	
	if(rLen!=m) 
		println("0") #code-word not valid
		exit(1)
	end

	

	vnodes=Dna2Dec(r)




	if(valid2(vnodes,H,cnodes))
		println("1 $n")	#code-word valid
	else
		println("0 $n")	#code-word not valid
	end



	
	
	


	



