#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

###################
#    Functions    #
###################




	function Dna2Dec(y)

		len=length(y)
		dec=[]
		for i=1:len
			if(y[i]=='A')
				push!(dec,0);
			elseif(y[i]=='C')
				push!(dec,1);
			elseif(y[i]=='G')
				push!(dec,2);
			elseif(y[i]=='T')
				push!(dec,3);
			end
		end

		return dec
	end

	function Dec2Dna(y)

		len=length(y)
		dna=[]
		for i=1:len
			if(y[i]==0)
				push!(dna,'A');
			elseif(y[i]==1)
				push!(dna,'C');
			elseif(y[i]==2)
				push!(dna,'G');
			elseif(y[i]==3)
				push!(dna,'T');
			end
		end

		return dna
	end



	#---
	#---  GF(4) Arithmetic 
	#---

	include("GF4Arithmetic.jl")



####################
#	Init()	   #
####################
	using MAT
	using DelimitedFiles


	q=4


	




	

####################
#	Main()	   #
####################

	

	if(length(ARGS)<2)
		println("error: NB-LDPC Encoder, not enough arguments !")
		exit(1)
	end


	matName=ARGS[2]
	ext=split(matName,'.')[end]


	if(ext=="mat" || ext=="m")
		file = matopen(matName)
		G = Int.(read(file, "G")) #parity check matrix
	else #assume a text file
		G=readdlm(matName) #"G_matrix_K250.txt"
		G=Int.(G)
	end


	

	m=length(G[1,:]) #columns
	n=length(G[:,1]) #rows


	fasta=readdlm(ARGS[1])
	r=""
	if(fasta[1,1]==">")
		r=fasta[2]
	else
		r=fasta[1]
	end

	rLen=length(r)

	if(rLen>=1)
		if(r[1]!='A' && r[1]!='C' && r[1]!='G' && r[1]!='T')
			println("error: unknown bases=",r[1]," detected! please put a right fasta file")
			exit(1)
		end
	else
		println("error: please put a right fasta file")
		exit(1)
	end

	

	if(rLen!=n) #information length is != from number of rows

		println("error: NB-LDPC Encoder, provided sequence length should be equal to ",n) #print that the word is not valid
		exit(1)

	end


	y=Dna2Dec(r)


	c=matrixGFMult(transpose(y),G)



	c=Dec2Dna(c)


	for i=1:m
		print(c[i])
	end

