#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

###################
#    Functions    #
###################





	#**connect v-nodes to c-nodes (each c-node will have indexes of all its v-nodes)**#
	function VToCnodes(H,cnodes)

		n=length(H[:,1]) #number of line or equations or c-nodes

		for i=1:n
			push!(cnodes,[]) #new equation or connexion
			m=length(H[i,:])
			for j=1:m
				if(H[i,j]!=0) #so the v-nodes is include on the parity check equation
					push!(cnodes[i],j) #push indexes of v-nodes
				end
			end
		end
	end



	
	function insertBase(y,H,cnodes,d,ls)
		#println("test")
		n=length(y)
		res=true

		while (d>0)
			scores=[]
			for i=1:ls:n-ls
				insert!(y,i,0)
				push!(scores,valid2(y,H,cnodes,0)[2])
				deleteat!(y,i)
			end
			max=argmax(scores) 
			if(max!=1)
				insert!(y,ls*max+1,0)
			else
				insert!(y,1,0)
			end
			pop!(y) #remove one of the extra elements add to allow PC equations computation 
			d=d-1;
		end

	end

	#Chack how many PC are correct
	function valid2(y,H,cnodes,iter)
		n=length(cnodes)
		res=true
		score=0 #score will be used to try to find best insertion position 

		for i=1:n
			m=length(cnodes[i])
			tmp=0 
			for j=1:m
				tmpM=multGF(y[cnodes[i][j]],H[i,cnodes[i][j]]) #coef mult
				tmp=addGF(tmp,tmpM); #+(GF4) of v-nodes j connected with c-node i
			end
			if(tmp!=0) 
				res=false
			else
				score+=1
			end
		end

		return res,score

	end

 
	function Dec2Dna(y)

		len=length(y)
		dna=[]
		for i=1:len
			if(y[i]==0)
				push!(dna,'A');
			elseif(y[i]==1)
				push!(dna,'C');
			elseif(y[i]==2)
				push!(dna,'G');
			elseif(y[i]==3)
				push!(dna,'T');
			end
		end

		return dna
	end


	function Dna2Dec(y)

		len=length(y)
		dec=[]
		for i=1:len
			if(y[i]=='A')
				push!(dec,0);
			elseif(y[i]=='C')
				push!(dec,1);
			elseif(y[i]=='G')
				push!(dec,2);
			elseif(y[i]=='T')
				push!(dec,3);
			end
		end

		return dec
	end




	#---
	#---  GF(4) Arithmetic 
	#---

	include("GF4Arithmetic.jl")

	using MAT
	using DelimitedFiles

####################
#	Init()	   #
####################


	if(length(ARGS)<2)
		println("error: NB-LDPC Decoder, not enough arguments provided !")
		exit(1)
	end



	r=""

	fasta=readdlm(ARGS[1])

	if(fasta[1][1]=='>' || fasta[1,1]=='>')
		if(length(fasta[2])>0)
			r=fasta[2]
		else
			println("error: NB-LDPC Decoder, please put a Fasta file !")
			exit(1)
		end
	else
		if(length(fasta[1])>0)
			r=fasta[1]
		else
			println("error: NB-LDPC Decoder, please put a Fasta file !")
			exit(1)
		end
	end
	
	rLen=length(r)



	H=[] #parity check matrix

	
	matName=ARGS[2]

	ext=split(matName,'.')[end]

	if(ext=="mat" || ext=="m")
		file = matopen(matName)
		H = Int.(read(file, "H"))
	else #assume a text file
		H = readdlm(matName)
		H = Int.(H) 	
	end

		
	

	m=length(H[1,:]) #columns
	n=length(H[:,1]) #rows

	cnodes=[]
	vnodes=[]



####################
#	Main()	   #
####################



	d=m-rLen #number of bases that should be added



	ls=trunc(Int64,(m*5)/100) #segments length (5% of the total length) 




	VToCnodes(H,cnodes) #init connexion v-nodes <-> cnodes

	vnodes=Dna2Dec(r)


	for i=rLen+1:m
		push!(vnodes,0)#add A's  for the rest of the sequence
	end


	res=valid2(vnodes,H,cnodes,0)[1]

	insertBase(vnodes,H,cnodes,d,ls)

	#remove extra bases added during first step i=rLen+1:m
	for i=m+1:length(vnodes)
		deleteat!(vnodes,i)
	end



	vnodes=Dec2Dna(vnodes)

	for i=1:m
		print(vnodes[i]) ;
	end

