%---------------------------------------------------------------------------#
%	@Authors: Elsa Dupraz, Belaid Hamoum 				    #
%	Project : DnarXiv, funded by CominLabs				    #
%	email   : belaid.hamoum@gmail.com				    #
%---------------------------------------------------------------------------#

function DNA_BP_LDPC_decoder(PathInOut,HMatrixFunc)
    % Test the *Non-binary* LDPC algorithm
    %***page_output_immediately(true)
    %***pkg load communications

    % *** Parameters ***
    % Number of input symbols, power of 2 (q : 2^q symbols)
    q = 2;
    % Probability distribution for the input symbols
    px = [0.25 0.25 0.25 0.25];
    % Probability distribution for the channel
    %pz = [0.91 0.03 0.03 0.03];
    
            %p(Y=A|X=A)
            %Y
%     pz = [0.98342075,0.00338629,0.0091428,0.00405016; %X
%           0.00446663,0.98274879,0.00370053,0.00908405; 
%           0.00860151,0.00315,0.98431995,0.00392854;
%           0.00498667,0.0100006,0.00433847,0.98067426];
      %pz=pz';

           %p(X=A|Y=A)
           %X
     pz= [0.903544 0.023989 0.044734 0.027734; %Y
           0.016697 0.917887 0.015575 0.049841;
           0.045891 0.018859 0.913311 0.021939;
           0.019837 0.043914 0.018730 0.917519;
           ] ;

      
    % Number of iterations
    it = 20;
    
    % Load the coding matrix %use eval to allow matrixName argument use
    %H = matrix()';
    [path,name,ext]=fileparts(HMatrixFunc);
    
    if(strcmp(ext,'.txt'))
        H=dlmread(HMatrixFunc);
    else
        %eval(['H = load(''' HMatrixFunc ''').H']); 
        eval(['H = load(''' HMatrixFunc ''')']); 
        H=H.H;
    end

    H=H'; %do it in prev instruction if possible

    % Sequence length
    N = length(H);

    % Build the class of parameters
    [param,cst] = c_param(q,px,pz,N,it,H);

   
    %==== read Reference file X to compare it with decoded seq OR DO it externally 
    %=====   [X] =  DO COMPARISON OUTSIDE TO SAVE TIME

    %==== Y :read consensus sequence in quaternary format ('A'->0, 'C'->1,...)
    strConsQuat=strcat(PathInOut,'/tmpConsQuatSeq.txt');
    formatSpec = '%d';
    fileID = fopen(strConsQuat,'r');
    Y= fscanf(fileID,formatSpec);
    
    % if consensus sequence shorter than what expected -> fullfill it with 0
    if(length(Y)<N) 
        Y(end:N)=0;
    end

    % *** Realizes the decoding of U ***
    U = zeros(param.M,1);

    % Initial matrix of messages from V.N. to C.N.
    % Rem : init_m is the function to adapt to the channel
    % Messages : log P(X|Y=0)/P(X|Y=k)
    m0 = init_m(param,Y);
    % Decoding
    %U=1:N
    [Xh,Xhval] = decode(U,m0,param,cst);

    %====Write Xh (estimated sequence ) on a file B.
    %Xh ->file
        strDecSeq=strcat(PathInOut,'/decodedSeq.txt');
        fileID = fopen(strDecSeq,'w');
        fprintf(fileID,'%d',Xh(1:end));
        fclose(fileID);
        
        
    %====Write 1 or 0 (estimated sequence valid or not) on a file B.
    %length(find(X-Xh))>0 => 0 Else 1 ->file

    % *** Computes the Error ***
    %Err = length(find(X-Xh))/param.N ;


    %***page_output_immediately(false)
end
