function [Err] = simu(param,cst)
% Simulation

% Sample sequences X and Y 
[X,Y] = gen_sources(param);

% Realizes the coding of X
U = encode(X,param);

% Realizes the decoding of U
% Initial matrix of messages from V.N. to C.N.
% Rem : init_m is the function to adapt to the channel
% Messages : log P(X|Y=0)/P(X|Y=k)
m0 = init_m(param,Y);
% Decoding
[Xh,Xhval] = decode(U,m0,param,cst);

% Computes the Error
Err = length(find(X-Xh))/param.N;


end
