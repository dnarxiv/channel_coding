function [X,Y] = gen_sources(param)
% Sample sequences X (source) and Y (Side information)
% param : see c_param.m

% For X
X_r = rand(param.N,1);
X = assoc(X_r,param.px)-1;

% Produces Z
Zr = rand(param.N,1);
Z = assoc(Zr,param.pz)-1;

% Produces Y
%**if(find(X>255) || find(Z>255))
%**	keyboard
%**end
Y = gf(X,param.q) + gf(Z,param.q);
Y = Y.x;

end



function [S] = assoc(vecteur,ps)
% vecteur : contains real values between 0 and 1 
% Associate to these values integer values, depending on ps 

% Initialization
alpha = 0;
S = zeros(size(vecteur));

% Association 
for k=1:length(ps)
        S(find((vecteur>alpha)&(vecteur<alpha+ps(k))))=k;
        alpha = alpha+ps(k);
end


end
