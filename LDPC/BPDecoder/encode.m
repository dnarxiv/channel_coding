function [U] = encode(X,param)
% Realizes the coding of X
% param : see c_param.m

U = gf(full(param.H)',param.q) * gf(X,param.q);
U = U.x;

end
