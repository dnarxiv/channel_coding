#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

BEGIN{
	split("",dnaSeq)
	}

{

	split($0,seq,"")
	
	for(i=1;i<=length(seq);i++)
	{
		if(seq[i]=="0")
			dnaSeq[i]="A"
		else if (seq[i]=="1")
			dnaSeq[i]="C"
		else if (seq[i]=="2")
			dnaSeq[i]="G"
		else if (seq[i]=="3")
			dnaSeq[i]="T"
	}

}

END{

	if(out!="")
	{
		printf("") > (out"decDNASeq.txt") #erase file
		for(i=1;i<=length(dnaSeq);i++)
			printf("%s",dnaSeq[i]) >> (out"decDNASeq.txt")
	}
	else
	{
		for(i=1;i<=length(dnaSeq);i++)
			printf("%s",dnaSeq[i]);
	}

}

