#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

BEGIN{
	split("",quatSeq)
	}

{
	split($0,seq,"")

	for(i=1;i<=length(seq);i++)
	{
		if(seq[i]=="A")
			quatSeq[i]="0"
		else if (seq[i]=="C")
			quatSeq[i]="1"
		else if (seq[i]=="G")
			quatSeq[i]="2"
		else if (seq[i]=="T")
			quatSeq[i]="3"
	}

}

END{

	if(out!="")
	{

		printf("") > ("./"out"tmpConsQuatSeq.txt") #erase file
		for(i=1;i<=length(quatSeq);i++)
		{
			printf("%c ",quatSeq[i]) >> ("./"out"tmpConsQuatSeq.txt")

		}
	}
	else
	{
		for(i=1;i<=length(quatSeq);i++)
			printf("%c",quatSeq[i]) 
	}
}
