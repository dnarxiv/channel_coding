#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

include("src/CC_Decoder_BSID_2_offset_MultSeq_joint_sep_logDomain_optimized_NBLDPC_parallel_forGitLab.jl")
include("src/CRC_Dec.jl")
include("src/randSeq_bin_10k.jl")
include("src/GF4_functions.jl")

#ARGS[1]: input path to multiple encoded binary sequences(string type) that represents the same original sequence e.g. : "100011", each line is a binary to decode
#ARGS[2]: output path to the consensus decoded binary sequence(string type) e.g. : "100011"
#ARGS[3]: expected i.e. Encoded Sequence Length (after CRC+CC encoder) [integer]
#ARGS[4]: boolean (0 or 1) to know if we add CRC or not

# exemple : julia Concat_CRC_CC_Dec.jl test_cluster.txt test_decoded_cluster.txt 52 0


if(length(ARGS) != 4)
	println("Usage: julia Concat_CRC_CC_Dec.jl input_path output_path encoded_len CRC(0/1)")
	exit(1)
end

input_path = ARGS[1]
output_path = ARGS[2]
expectedLen=parse(Int,ARGS[3])
enable_CRC=parse(Bool,ARGS[4]) #Remove CRC if enabled during encoding or not (0-> remove, 1 ->nop)


# get the binary sequences in each lines of the input file
open(input_path) do source
	global Seqs = readlines(source) # sequence(s) (in binary) to decode, e.g. : ["100011","001010","1100101",...] [array of strings] 
end


#Expr_memory_channel
Pi=0.009548 #Probability of insertion 
Pd=0.053559 #Probability of deletion
Ps=0.044976 #Probability of substitution
Pm=0.891917 #Probability of match

apriori=[]   #Except for LDPC+CC, useless
decType="s"  #separate decoding. remark:joint decoding "j" is disabled
nbrSeqs=length(Seqs) # number of sequences in input

# apply CC decoding
y,LLR=cc_decoder_call(Seqs,decType,nbrSeqs,expectedLen,Pd,Pi,Ps,Pm,apriori)


# save the result to the output file
output = open(output_path, "w")

if(enable_CRC)
	# CRC checked & removed, valid will be true of false
	y_CC_CRC,valid=CRC_Decode(y) 
	println(output, y_CC_CRC," ",valid)
else
	# no CRC decoding, so valid is x for "undefined"
	println(output, y) #," x")
end





