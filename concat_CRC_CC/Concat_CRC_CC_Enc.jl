#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

include("src/CRC_Enc.jl")
include("src/CC_encoder_offset.jl")
include("src/randSeq_bin_10k.jl")
include("src/GF4_functions.jl")

#ARGS[1]: input path to binary sequences(string type) e.g. : "100011", each line is a binary to encode
#ARGS[2]: output path to binary sequences(string type) e.g. : "100011", each line is an encoded binary
#ARGS[3]: boolean (0 or 1) to know if we add CRC or not

# exemple : julia Concat_CRC_CC_Enc.jl exemple_input.txt test_output.txt 0

if(length(ARGS) != 3)
	println("Usage: julia Concat_CRC_CC_Enc.jl input_path output_path CRC(0/1)")
	exit(1)
end

input_path = ARGS[1]
output_path = ARGS[2]
enable_CRC = parse(Bool,ARGS[3])

output = open(output_path, "w")

# get the binary sequences in each lines of the input file
open(input_path) do source
	for line in eachline(source)

		if(enable_CRC) # apply Cyclic Redundancy Check encoder to the binary string
			line = CRC_Encode(line)
		end
			
		x_final = CCEncoder_Call(line) # apply the Convolutional Code encoder to the binary string

		println(output, x_final)
	end
end

close(output)

println("\tChannel encoding completed !")
