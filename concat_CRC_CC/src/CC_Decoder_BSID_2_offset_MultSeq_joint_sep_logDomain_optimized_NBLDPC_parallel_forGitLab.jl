#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#



struct state
       id::Array{Int64,1}		# current state [[00] [01] [10] [11]] (only one state)
       inputs::Array{Array{Int64,1},1}	# All possible inputs[0 1], for each input its own output located at the same index in outputs field
       outputs::Array{Array{Array{Int64,2},1},1}	# All possible outputs [[0 0] [1 1] ...] all possible outputs depending on inputs
       nextStates::Array{Array{Int64,1},1}		# All possible next states [[00] [01] [10] [11]] (go to the right one depending on input and currState)
end



mutable struct edges #mutable means fields can be modified
	going  #states that will be reached from state i
	gammas_going  #each value belongs to the edge located at the same index than the inputIndex
end

mutable struct node #mutable means fields can be modified
	id::Array{Int64,1}	# current state [[00] [01] [10] [11]] (only one state)
	d::Int64		# drift value
	alpha::Array{Float64,2}	# forward recursion value on this node , use array that contains alpha related for each input and the sum ( make easy LLRs computation (depends on inputs) )
	beta::Array{Float64,2}			# backward recursion value on this node, use array that contains alpha related for each input and the sum ( make easy LLRs computation (depends on inputs) )
	edges::edges
	#inputs,nextNodeIndex : Use the state diagram to find which is next state to reach depending on the input
end
mutable struct nodeJoint #mutable means fields can be modified
	id::Array{Int64,1}	# current state [[00] [01] [10] [11]] (only one state)
	d::Array{Int64,1}		# drift value
	alpha::Array{Float64,2}	# forward recursion value on this node , use array that contains alpha related for each input and the sum ( make easy LLRs computation (depends on inputs) )
	beta::Array{Float64,2}			# backward recursion value on this node, use array that contains alpha related for each input and the sum ( make easy LLRs computation (depends on inputs) )
	alphaBool::Bool		# To know if alpha and beta of the state were already computed ('1') or not ('0') to avoid redundant computation
	betaBool::Bool		# To know if alpha and beta of the state were already computed ('1') or not ('0') to avoid redundant computation
	edges::edges
	#inputs,nextNodeIndex : Use the state diagram to find which is next state to reach depending on the input
end



#############
#REMOVE OFFSET
#y_seg:segment ; t,n,begSeg,endSeg : stage, codeword len,  value to compute position of the offset sequence.
function removeOffset(y_seg,t,n)	


	len_y_seg=length(y_seg)
	tmpBinArray=zeros(Int8,len_y_seg)
	for tmpI=1:len_y_seg
		if(y_seg[tmpI]=='1')
			tmpBinArray[tmpI]=1
		else # ==0
			tmpBinArray[tmpI]=0
		end
	end

	begOffset=(t-1)*n+1
	offset=randSeq[begOffset:begOffset+(len_y_seg-1)]


	y_seg=join(xor.(tmpBinArray,offset)) #result as a string

	return "$y_seg"


end

#D:is a matrix that contains all generator polynomes
#states: vector of vectors containing all possible states
#L:constrained length
#k:inputLength
#q:alphabet length
#seqLen:sequence to decode length

function GenerateTrellis(stateDiagram,D,L,k,q,n,ySeq,dMin_ref,dMax_ref,nbrSeqToProcess,RefLen,Pd) #generate allpossible combination for a vector of length l, and alphabet q

	indexDict=[] #usefull to find where drift are located and don't compare all the structure

	trellis=[] #will contain all possible states (state structure) & their details (transitions, inputs/outputs)
	t=[] #all nodes at the instant t[i]


	edges_going=[]

	edges_ref=[] #contains going edges for state i (i=index), this way we avoid unecessary redundancy on the trelllis


	for i=1:length(stateDiagram[:,1])
		push!(edges_going,[])
	end

	nbrStates=length(stateDiagram[:,1])
	inList=GenerateCmb(k,q) ##all possible inputs
	


	gammas=0

	
	dMin=0
	dMax=0

	lenY=zeros(1,nbrSeqToProcess)

	if(typeof(ySeq)!=String)
		for i=1:nbrSeqToProcess
			lenY[i]=length(ySeq[i])
		end

	else

		lenY[1]=length(ySeq)
	end

	maxLen=lenY[argmax(lenY)]
	minLen=lenY[argmin(lenY)]



	#*nbrCodeWords=length(y)/n USE THE SECOND ONE TO ALWAYS GET THE RIGHT LENGTH STRUCTURE-------- 
	nbrCodeWords=RefLen/n
	lastDriftMax=maxLen-RefLen#to knowif will endup there or not
	lastDriftMin=minLen-RefLen#to knowif will endup there or not
	

	nbrCodeWords_Trunc=trunc(Int64,nbrCodeWords)

	if(nbrCodeWords==nbrCodeWords_Trunc)

		nbrCodeWords_Trunc=nbrCodeWords_Trunc+1 #+1 as it will end up in some state after generating last output
	else

		nbrCodeWords_Trunc=nbrCodeWords_Trunc+2 #add another stage to contain the last part and & +1 as it will end up in some state after generating last output
	end



	if(lastDriftMin<0 )
		if((dMin_ref[1]*nbrCodeWords_Trunc)>lastDriftMin) #increase dMin
			dMin_ref[1]= -(ceil(Int,(-lastDriftMin)/nbrCodeWords_Trunc))
		end
	end
	if(lastDriftMax>0)
		if((dMax_ref[1]*nbrCodeWords_Trunc)<lastDriftMax) #increase dMin
			dMax_ref[1]=ceil(Int,lastDriftMax/nbrCodeWords_Trunc)

		end
	end

	#thresholdDrifts=((nbrCodeWords_Trunc*n)/2)*(Pd/(1-Pd))
	thresholdDrifts=5*sqrt((maxLen)*(Pd/(1-Pd)))
	#*thresholdDrifts=5*sqrt((nbrCodeWords_Trunc*n)*(Pd/(1-Pd)))


	#In case threshold is too short to reach the last position of the sequence scale it
	if(thresholdDrifts<abs(lastDriftMax))
		thresholdDrifts=abs(lastDriftMax)
	end

	#turn next states values into index to easyli reach nodes
	nextNodesIndex=[] #index to reach next nodes
	for i=1:nbrStates		
		for jj=1:length(stateDiagram[i].nextStates)

				tmpIndex=findIndex(stateDiagram[i].nextStates[jj],L,q)	
				push!(nextNodesIndex,tmpIndex)	

				push!(edges_going[i],tmpIndex)
			
		end
	end

	for j=1:nbrCodeWords_Trunc
		push!(indexDict,[]) #stage
		t=[] #all nodes at the instant t[i]
		for i=1:nbrStates
			push!(indexDict[j],[])#state
			stateDrifts=[]


			
			
			#push!(t,node(stateDiagram[i].id,alpha,beta,alphaBool,betaBool,edges(nothing,nothing,[],[])))
			
			#I could add here some restrictions about drift depending on the stage *******
			indexes_idx=1	
			for d=dMin:dMax	
				indexes=[]
				push!(indexes,indexes_idx)									
				#create here to get another vector for each node instead of a reference pointing to the same array
				alpha=zeros(1,q+1) #init to 0
				alpha.=-Inf #LOG DOMAIN
				beta=zeros(1,q+1)
				beta.=-Inf #LOG DOMAIN


				gammas_going=[[],[]]


				going=[[],[]]	

		
				push!(stateDrifts,node(stateDiagram[i].id,d,alpha,beta,edges(going,gammas_going)))

				indexes_idx+=1
			
				if(!isempty(indexDict[j][i]))
					entry=Dict(d=>indexes)

					indexDict[j][i]=merge(indexDict[j][i],entry)

				else
					entry=Dict(d=>indexes)
					indexDict[j][i]=entry
				end

			end

			push!(t,stateDrifts)
			#create & provide state details &transitions 
			#println("input:",inputs)
			#println("output:",outputs)
	   
		end

		if(dMin>-thresholdDrifts)
			dMin=dMin_ref[1]+dMin

		end
		if(dMax<thresholdDrifts)
			dMax=dMax_ref[1]+dMax

		end

		push!(trellis,t)

	end


		edges_ref=edges(edges_going,nothing)


	return trellis,edges_ref,thresholdDrifts,indexDict

end
function innerConnectNodes(t,stateDiagram,trellis,dMin_ref,dMax_ref,edges_ref,thresholdDrifts,indexDict,nbrStates)
	for s=1:nbrStates	
			nbrEdges=length(edges_ref.going[s])
			nbrDrift=length(trellis[t][s])

			for b=1:nbrEdges

				nextStateIndex=edges_ref.going[s][b] # state index that will be reeached by s
				nextStateVect=stateDiagram[nextStateIndex].id #vector format of the id
				indexInOut=findall(x->x==nextStateVect,stateDiagram[s].nextStates) #index to reach the input that allows to go from s to currStates
				u=stateDiagram[s].inputs[indexInOut][1][1]+1


				for d1=1:nbrDrift

					drift=trellis[t][s][d1].d

					dmin=drift+dMin_ref[1]
					dmax=drift+dMax_ref[1]


					thresholdRange=false #if min or max threshold reached uses indexes pointer of match

					if(dmin<-thresholdDrifts)
						dmin=trellis[t+1][nextStateIndex][1].d
						thresholdRange=true
					end
					if(dmax>thresholdDrifts)
						dmax=trellis[t+1][nextStateIndex][end].d
						thresholdRange=true
					end


					firstCmpr=true #after the first comparison, reuse the deduced indexes
					indexesPointer=[] #find the right indexes during first nbrEdit iteration & then use them directly instead of comparing again

					for d2=dmin:dmax

					
						nbrIdx=0

						indexes=getindex(indexDict[t+1][nextStateIndex],d2)
						if(firstCmpr)
							nbrIdx=length(indexes)
						else
							nbrIdx=length(indexesPointer)
						end

						for idx=1:nbrIdx

								index2=indexes[idx]
								if(firstCmpr)
									
									push!(trellis[t][s][d1].edges.going[u],[nextStateIndex,index2])

									#init gammas going 
									push!(trellis[t][s][d1].edges.gammas_going[u],0.0)

									
									push!(indexesPointer,idx) #save arguments index						

								else
									pointer=indexesPointer[idx]

									push!(trellis[t][s][d1].edges.going[u],[nextStateIndex,indexes[pointer]])
									#init gammas going 
									push!(trellis[t][s][d1].edges.gammas_going[u],0.0)	
								end							
							
						end

						if(!thresholdRange )
							firstCmpr=false
						else
							firstCmpr=true
							indexesPointer=[]		
						end




					end

					#*end
				end

			end
				
	end

end

#function to connect each nodes with his possible transitions
#At this point, unused node (reach max values) should be removed
function connectNodes(stateDiagram,trellis,dMin_ref,dMax_ref,edges_ref,thresholdDrifts,indexDict)

	nbrStages=length(trellis)
	nbrStates=length(stateDiagram)


	Threads.@threads for t=1:(nbrStages-1)
	#for t=1:(nbrStages-1) if copy is not used
		# CHECK TO START USING COPY INSTEAD OF THE COMPUTATION

		innerConnectNodes(t,stateDiagram,trellis,dMin_ref,dMax_ref,edges_ref,thresholdDrifts,indexDict,nbrStates)

	
	end


end

#compute probability to observe y_seg given x through a latice
function laticeBSID(x,y_seg,PdLog,PiLog,PsLog,PmLog,PtLog,P1OverQ,PQMinus1,lastSection,q)





	#create latice as a multidimensional array, +1 to init first cell latice [1,1]
	#We will deal with symbols -> shift to the right (division) the length of x)
	latice=zeros((length(x)>>(q-1))+1,(length(y_seg)>>(q-1))+1)

	latice[1,1]=0 #Latice will be in log domain (=1 in prob domain)
	lenC=length(latice[1,:])
	lenL=length(latice[:,1])

		
	#init 1st line
	for i=2:lenC

		INS=+PiLog+latice[1,i-1]
		DEL=-Inf
		MATCH=-Inf	
		latice[1,i]=INS
	end
	#init 1st columns
	for i=2:lenL
		INS=-Inf
		DEL=PdLog+latice[i-1,1]
		MATCH=-Inf	
		latice[i,1]=DEL
	end


	ki=1# use it to go through x
	for i=2:lenL
		kj=1# use it to go through y_seg
		for j=2:lenC
			if(join(x[ki:ki+(q-1)]) == join(y_seg[kj:kj+(q-1)]) ) #Match

				INS=P1OverQ+PiLog+latice[i,j-1]
				DEL=PdLog+latice[i-1,j]
				MATCH= PtLog+PmLog+latice[i-1,j-1]


				if(!lastSection)
					latice[i,j]=maxStar(maxStar(INS,DEL),MATCH) 
				else
					 INS=-Inf #FOR BSID CHANNEL INS not allowed at the end
					latice[i,j]=maxStar(maxStar(INS,DEL),MATCH) 
				end

			else #substitution

				INS=P1OverQ+PiLog+latice[i,j-1]
				DEL=PdLog+latice[i-1,j]
				SUB=PtLog+(PsLog-PQMinus1)+latice[i-1,j-1]
							

				if(!lastSection)
					latice[i,j]=maxStar(maxStar(INS,DEL),SUB)  
				else
					INS=-Inf
					latice[i,j]=maxStar(maxStar(INS,DEL),SUB)  
				end

			end
			kj+=q
		end
		ki+=q
	end

	


	return latice[lenL,lenC],latice
	
end

function maxStar(x1,x2)

	if ( (x1 != -Inf) || (x2 != -Inf) )
		maxTerm=max(x1,x2)
		correctionTerm=log(1+exp(-abs(x1-x2)))
		return maxTerm+correctionTerm
	else
		return -Inf
	end

end
function innerGammas(t,stateDiagram,trellis,ySeq,n,q,nbrSeqToProcess,edges_ref,thresholdLimit,LLR,usePrevLLRs,dMin_ref,dMax_ref,PdLog,PiLog,PsLog,PmLog,PtLog,P1OverQ,PQMinus1,aprioriLog,nbrStates,nbrStages,lenY)

	latice=[]

	for k=1:nbrSeqToProcess
		push!(latice,[])
	end

	for s=1:nbrStates

		nbrDrifts=length(trellis[t][s])

		for d=1:nbrDrifts

			for u=1:2 # u=1 -> bits '0' , u=2 -> bit '1'

				nbrEdges=length(trellis[t][s][d].edges.going[u])
				alreadyComputed=Int.(zeros(1,nbrSeqToProcess)) #used to check if longest drift on the sequence already computed

				for b=nbrEdges:-1:1
					
					nextStateIndex=trellis[t][s][d].edges.going[u][b][1]  # state index that will be reeached by s ([1])
					nextDriftIndex=trellis[t][s][d].edges.going[u][b][2]  # drift of the state that will be reached ([2])

					uValue=u
					x=stateDiagram[s].outputs[u] #pop the x value (was a vector on a vector)
					x=x[1]
					y_seg=""


					currDrift=trellis[t][s][d].d
					NextDrift=trellis[t+1][nextStateIndex][nextDriftIndex].d	
					begSeg=(t-1)*n+(currDrift*q)+1   #to  start from first char ,+ 1 to compensate fact that index don't start from 0 
					endSeg=(t)*n+(NextDrift*q)     #to endup at char before , no +1 because last index taken into account too (kind of +1)





					gamma=0 

					for seqNum=1:length(currDrift) #currDrift contains the drifts related to each sequence

					#gamma=p(y|x).p(u)

						if(alreadyComputed[seqNum]>0)

							Pyx=-Inf
							pos=NextDrift[seqNum]-currDrift[seqNum]+2

							if(pos>0)
								Pyx=(latice[seqNum][end,pos]+aprioriLog[t])
							elseif(pos==dMin_ref[1]) #only 1 del should be allowed (there couldn't be two deletion at the same symbol...)
								Pyx=(latice[seqNum][end,1]+aprioriLog[t])
							else
								Pyx=-Inf
							end
							gamma=gamma+Pyx

						else
							if(begSeg<=0)
								begSeg=1
							end
							if(lenY[seqNum]>=endSeg)

								if(typeof(ySeq)==String)
									y_seg=ySeq[begSeg:endSeg]
								else
									y_seg=ySeq[seqNum][begSeg:endSeg]
								end

							elseif(lenY[seqNum]>=begSeg) #

								gamma=-Inf #

								continue

							elseif(lenY[seqNum]<begSeg) # Ignore this prob as it shouldn't exist in this sequence?
																						
								gamma=-Inf #

								continue

							end


							#REMOVE OFFSET
							y_seg=removeOffset(y_seg,t,n)	


							##############
							# BSID channel calibrated to the memory channel model
							##############
							
							Pyx=-Inf
							lastSection=false

							#=	
								if(t<nbrStages-1)
									latice[seqNum]=laticeBSID(x,y_seg,PdLog,PiLog,PsLog,PmLog,PtLog,P1OverQ,PQMinus1,lastSection,q)[2]
								else
									lastSection=true
									latice[seqNum]=laticeBSID(x,y_seg,PdLog,PiLog,PsLog,PmLog,PtLog,P1OverQ,PQMinus1,lastSection,q)[2]
								end									
							=#				
							latice[seqNum]=laticeBSID(x,y_seg,PdLog,PiLog,PsLog,PmLog,PtLog,P1OverQ,PQMinus1,lastSection,q)[2] #in mem channel, possible to get INS at the end
							Pyx=latice[seqNum][end,end]+aprioriLog[t]

							gamma=gamma+Pyx
							alreadyComputed[seqNum]=1

						end
					

					end
					

					trellis[t][s][d].edges.gammas_going[uValue][b]=gamma   
				
					
				end

			end
		end
	end

end
	
function compute_gammas(stateDiagram,trellis,ySeq,n,q,nbrSeqToProcess,edges_ref,thresholdLimit,LLR,usePrevLLRs,dMin_ref,dMax_ref,PdLog,PiLog,PsLog,PmLog,PtLog,P1OverQ,PQMinus1,)




	      #0  #1
	aprioriLog=zeros(1,length(trellis))
	      #0  #1
	halfLog=0

	if(usePrevLLRs)
		aprioriLog=LLR
		
	else
		aprioriLog.=halfLog
	end

	
	nbrStages=length(trellis)
	nbrStates=length(stateDiagram)
	

	lenY=Int.(zeros(1,nbrSeqToProcess))

	if(typeof(ySeq)!=String)

		for i=1:nbrSeqToProcess
			lenY[i]=length(ySeq[i])
		end
	else
		lenY[1]=length(ySeq)
	end
		



	

	Threads.@threads for t=1:(nbrStages-1)
	innerGammas(t,stateDiagram,trellis,ySeq,n,q,nbrSeqToProcess,edges_ref,thresholdLimit,LLR,usePrevLLRs,dMin_ref,dMax_ref,PdLog,PiLog,PsLog,PmLog,PtLog,P1OverQ,PQMinus1,aprioriLog,nbrStates,nbrStages,lenY)		
		
	end	
end





function compute_alphasNew(stateDiagram,trellis,ySeq,iter,nbrStages) 

	for t=0:(nbrStages-1)

		nbrStates=length(stateDiagram)

		if(t>0)

			for s=1:nbrStates

				nbrDrifts=length(trellis[t][s])

				for d=1:nbrDrifts

					for u=1:2 # u=1 -> bits '0' , u=2 -> bit '1'


						nbrEdges=length(trellis[t][s][d].edges.going[u])

						for j=1:nbrEdges


							goingEdge=trellis[t][s][d].edges.going[u][j]
							gamma=trellis[t][s][d].edges.gammas_going[u][j]
							nextState=goingEdge[1]
							nextDrift=goingEdge[2]

							tmp=-Inf
							if(trellis[t][s][d].alpha[end]==-Inf)
								tmp= -Inf #gamma is already in log mode

							else
								tmp=trellis[t][s][d].alpha[end]+gamma #gamma is already in log mode

							end


							trellis[t+1][nextState][nextDrift].alpha[u]=maxStar(trellis[t+1][nextState][nextDrift].alpha[u],tmp)
							trellis[t+1][nextState][nextDrift].alpha[end]=maxStar(trellis[t+1][nextState][nextDrift].alpha[end],tmp)

						
						end
						
					end
				end
			end
			

		else #t==1
				

			for s=1:nbrStates

				nbrDrifts=length(trellis[t+1][s])

				for d=1:nbrDrifts				
					#init alphas:
					#In an all zeros terminated trellis alpha(S)={1 when S=0, 0 when S!=0}  no need to check drift value as there is only a drift of "0" at 1st stage
					if(s==1 && d==1)
						trellis[t+1][s][d].alpha.=0

					else#	(s!=1)

						trellis[t+1][s][d].alpha.=-Inf
					end
				end
			end
		end
	


	end


end


function compute_betasNew(stateDiagram,trellis,ySeq,q,iter,nbrStages,nbrSeqToProcess,dMin_ref,dMax_ref,edges_ref,thresholdLimit,RefLen) #nbr of stages (times moments) and avoid to compute length(trellis) each time
	for t=nbrStages:-1:1

		nbrStates=length(stateDiagram)
		if(t<nbrStages)
			for s=1:nbrStates
				nbrDrifts=length(trellis[t][s])

				for d=1:nbrDrifts



					for u=1:2 # u=1 -> bits '0' , u=2 -> bit '1'

						nbrEdges=length(trellis[t][s][d].edges.going[u])


							for j=nbrEdges:-1:1


								goingEdge=trellis[t][s][d].edges.going[u][j]

								gamma=trellis[t][s][d].edges.gammas_going[u][j]
								nexState=goingEdge[1]
								nextDrift=goingEdge[2]
								
								tmp=0
								if(trellis[t+1][nexState][nextDrift].beta[end]==-Inf)
									tmp=-Inf
								else
									tmp=trellis[t+1][nexState][nextDrift].beta[end]+gamma
								end

								

								trellis[t][s][d].beta[u]=maxStar(trellis[t][s][d].beta[u],tmp)
											
								trellis[t][s][d].beta[end]=maxStar(trellis[t][s][d].beta[end],tmp)
							
										
							end
						
					end

				end
			end


		else #t==nbrStages
			#init betas:
			#In an all zero terminated trellis beta(S)={1 when S=0,d=finalDrift, 0 when S!=0}
			
			finalDrift=[]
			if(typeof(ySeq)!=String)
				for i=1:nbrSeqToProcess
							  #divide by q because I  consider symbols (1symbold =1drift=2bits, q=2)
					push!(finalDrift,(length(ySeq[i])-RefLen)>>(q-1))
				end	
			else

				push!(finalDrift,(length(ySeq)-RefLen)>>(q-1))

			end

			for s=1:nbrStates

				nbrDrifts=length(trellis[t][s])

				for d=1:nbrDrifts



					drift=trellis[t][s][d].d

					if(s!=1 || [drift]!=finalDrift )

						trellis[t][s][d].beta.=-Inf

					else


						trellis[t][s][d].beta.=0

					end	
				end
			end


		end

		#*normalizeStageBeta(trellis,t) 

	end

end

function innerCompute_LLRs(t,stateDiagram,trellis,nbrStages,nbrStates,LLR)
	nominator=-Inf
	denominator=-Inf


	for s=1:nbrStates

		nbrDrifts=length(trellis[t][s])

		for d=1:nbrDrifts

			for u=1:2 # u=1 -> bits '0' , u=2 -> bit '1'

				nbrEdges=length(trellis[t][s][d].edges.going[u])

				for j=nbrEdges:-1:1

					nextState=trellis[t][s][d].edges.going[u][j][1]
					nextDrift=trellis[t][s][d].edges.going[u][j][2]

				
					if(u==2) # u=1 edges 

						a=trellis[t][s][d].alpha[end];
						b=trellis[t+1][nextState][nextDrift].beta[end];
						g=trellis[t][s][d].edges.gammas_going[u][j]

						tmp=(a+g+b)
						nominator=maxStar(nominator,tmp)
						
					elseif(u==1) # u=0 edges

						a=trellis[t][s][d].alpha[end];
						b=trellis[t+1][nextState][nextDrift].beta[end];
						g=trellis[t][s][d].edges.gammas_going[u][j]

						tmp=(a+g+b)
						denominator=maxStar(denominator,tmp)
						
					end
						

				
				end
			end
		end

	end



	LLR[t]=nominator-denominator

end

function compute_LLRs(stateDiagram,trellis,nbrStages) #adapted for binary use only

	nbrStates=length(stateDiagram)

	LLR=zeros(1,nbrStages-1)

	Threads.@threads for t=1:nbrStages-1

		innerCompute_LLRs(t,stateDiagram,trellis,nbrStages,nbrStates,LLR)
	end

	return LLR

end

function LLRs2Val(LLR,stateDiagram,L) #adapted for binary use only
	yDec=""

	len_LLR=length(LLR)-L #last L bits are termination bits
	#LLRS to bits
	for i=1:len_LLR
		if(LLR[i]>0) #1
			yDec="$(yDec)$(stateDiagram[1].inputs[2][1])"
		elseif(LLR[i]<0)#0
			yDec="$(yDec)$(stateDiagram[1].inputs[1][1])"
		end
	end

	return yDec
end


function CC_BCJR_Decoder_Separate(stateDiagram,q,D,k,L,n,y,decType,dMin_ref,dMax_ref,nbrSeq,Pd,Pi,Ps,Pm,RefLen,apriori) #generate allpossible combination for a vector of length l, and alphabet q
	#init probs in log domain
	PiLog=log(Pi)
	PdLog=log(Pd)
	#Pt=1-Pi-Pd
	#PtLog=log(Pt)
	PtLog=0 #ignored because of the considered channel
	PsLog=log(Ps) 
	#PmLog=log(1-Ps)  #match
	PmLog=log(Pm)  #match
	P1OverQ=log(1/q)
	PQMinus1=log(q-1)


	LLR=zeros(1,Int(ceil(RefLen/n)))
	ySeq=""
	usePrevLLRs=false

	for i=1:nbrSeq

		nbrSeqToProcess=1
		if(nbrSeqToProcess==1)
			if(typeof(y)!=String)
				ySeq=y[i]
			else
				ySeq=y
			end
		else
			ySeq=y
		end
		if(usePrevLLRs)
			LLR=apriori
		end


	#print("\ngenerateTrellis:") @time
		 trellis,edges_ref,thresholdDrifts,indexDict=GenerateTrellis(stateDiagram,D,L,k,q,n,ySeq,dMin_ref,dMax_ref,nbrSeqToProcess,RefLen,Pd)
		thresholdDrifts=round(Int,thresholdDrifts)
	#print("\nconnexions:")@time
		 connectNodes(stateDiagram,trellis,dMin_ref,dMax_ref,edges_ref,thresholdDrifts,indexDict)

		nbrStages=length(trellis) #depth

	#print("\ngammas:")@time
		#y contain a vector of sequences to decode
		#compute_gammas(stateDiagram,trellis,y,n)
		 compute_gammas(stateDiagram,trellis,ySeq,n,q,nbrSeqToProcess,edges_ref,thresholdDrifts,LLR,usePrevLLRs,dMin_ref,dMax_ref,PdLog,PiLog,PsLog,PmLog,PtLog,P1OverQ,PQMinus1)



		Threads.@threads for z=1:2 #Tricky way to execute the functions in parallel
			if(z==1)
				iter=nbrStages;
				#print("\nalphas:")@time
				 compute_alphasNew(stateDiagram,trellis,ySeq,iter,nbrStages)
			elseif(z==2)
				iter=1;
				#print("\nbetas:")@time
				 compute_betasNew(stateDiagram,trellis,ySeq,q,iter,nbrStages,nbrSeqToProcess,dMin_ref,dMax_ref,edges_ref,thresholdDrifts,RefLen)
			end
		end
		#*println("trellis:",trellis[1])
		#println("----------")
		#println(trellis[4][1])

		#***********normalize(trellis) 


		#LLR=LLR.*compute_LLRs(stateDiagram,trellis,y[i],nbrStages)
		#print("\nLLR:") @time
		 LLR=LLR+compute_LLRs(stateDiagram,trellis,nbrStages)

	end

	yDec=LLRs2Val(LLR,stateDiagram,L)
	
	return yDec,LLR

end

function GenerateCmb(len,q) #generate allpossible combination for a vector of length l, and alphabet q

	Cmb=[] #all possiblecombinations
	symbolShot=trunc(Int,(q^len)/q) #number of same elements (will be updated at each column
	nbrStates=q^len

	for i1=1:len #L:constraint length
		i2=1
		while (i2<nbrStates)	#number of states
			for i3=0:q-1 #alternate between symbols
				for i4=1:symbolShot #swap shot[number of same symbols that will be used on one shot] ex: column1: 0 0 0 0 1 1 1 1, column2: 0 0 1 1 0 0 1 1, ... 
					if(i1>1)
						push!(Cmb[i2],i3)				
					else

						push!(Cmb,[i3])				
					end
					i2=i2+1
				end
			end
		end

		symbolShot=trunc(Int,symbolShot/(q))
	end


	return Cmb
end


#D:is a matrix that contains all generator polynomes
#states: vector of vectors containing all possible states
#L:constrained length
#k:inputLength
#q:alphabet length
function GenerateStateDiagram(states,D,L,k,q) #generate allpossible combination for a vector of length l, and alphabet q
	diagram=[] #will contain all possible states (state structure) & their details (transitions, inputs/outputs)


	inList=GenerateCmb(k,q) ##all possible inputs

	for i=1:length(states[:,1])
		s=copy(states[i,:]) 
		s=pop!(s) #pop is used to extract only one vector "[0,1,2]" instead of a vector in a vector "[[0,1,2]]"
		inputs=[]
		nextStates=[]
		outputs=[]
		for j=1:length(inList[:,1])
			input=copy(inList[j,:])
			input=pop!(input)

			push!(inputs,input)
			tmpOutputs=[]

			tmpIn=[inputs[j];s] #concat(merge) input & states to compute output  ??????????????????????
			
			push!(tmpOutputs,matrixGFMult(transpose(tmpIn),transpose(D)))
			push!(nextStates,tmpIn[1:end-k]) # shift 
			push!(outputs,tmpOutputs)
			
		end
		
		#create & provide state details &transitions 
		#println("input:",inputs)
		#println("output:",outputs)
		push!(diagram,state(s,inputs,outputs,nextStates))
		
   
	end

	return diagram
end

function findIndex(vect,Len,q)
	
	symbolShot=trunc(Int,(q^Len)/q) #number of same elements (will be updated at each column

	index=0
	for i1=1:Len #Len:constraint length(L) or any other length
		index=index+(vect[i1]*symbolShot)
		symbolShot=trunc(Int,symbolShot/(q))
	end

	return (index+1) #otherwise first index will be 0

end

function checks(stateDiagram,seq,k,L,q)
	nbrStates=length(stateDiagram)

	if(nbrStates<=0)
		println("Exit ERROR: Empty state diagram!")
		exit(1)
	end
	
	l=length(seq)
	resDiv=l/k
	if(resDiv!=trunc(Int,resDiv))
		println("Exit ERROR: Sequence to encode length should be a multiple of k=",k,"!")
		exit(1)
	end

end

function initCCDecoder(D,L,k,q,seq)

	states=GenerateCmb(L,q)

	stateDiagram=GenerateStateDiagram(states,D,L,k,q)  #stateIndex will be used to easily locate the current state entry on a vector

	checks(stateDiagram,seq,k,L,q)

	return  stateDiagram


end


#############################
#	MAIN ()  
############################


##CC_Decoder.jl $seq 

function cc_decoder_call(Y,decType,nbrSeq,expectedLen,Pd,Pi,Ps,Pm,apriori)

	if(length(Y)==0)
		println("error: no input provided!")
		exit(1)
	end

	RefLen=expectedLen#256 #USED TO KNOW on which drift TO INIT betas to 1!!!!!

	#CC_parameters
	k=1; #input length
	m=3; # memory length (nbr of registers)
	n=2; #output length 
	q=2; #alphabet size
	L=k*(m-1) ; #constraint length
	D=[1 0 1; 1 1 1 ] #polynomial generator

	nbrStates=q^L;

	dMin_ref=[-1]#-1
	dMax_ref=[2]#+1

	#x=1011101001110011000100101001001
	#y="110100100110000111111001101111101011001101111101000111110111110111"

	#y=ARGS[1]
	y=Y



	#load variable "randSeq" which is a sequence of 10k binary simbols


	stateDiagram=initCCDecoder(D,L,k,q,y)


	#decType: 1 -> joint decoding, 0 -> separate decoding
	decType=decType

	if(decType=="j") #joint decoding
		#println("Joint decoding:")
		#yDec,LLR=CC_BCJR_Decoder_Joint(stateDiagram,q,D,k,L,n,y,decType,dMin_ref,dMax_ref,nbrSeq,Pd,Pi,Ps,RefLen,apriori) 
		println("Error: joint decoding functions not available!")
	elseif(decType=="s") #separate decoding
		#println("Separate decoding:")
		yDec,LLR=CC_BCJR_Decoder_Separate(stateDiagram,q,D,k,L,n,y,decType,dMin_ref,dMax_ref,nbrSeq,Pd,Pi,Ps,Pm,RefLen,apriori)
	else #error 
		println("Error: please specify a 'j' (joint decoding) or a 's' (separate decoding) option.")
		exit(1)
	end


	#println("LLR=",LLR)
	#println(yDec)
	return yDec,LLR

end


