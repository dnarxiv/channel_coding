#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

using CRC32c



#seq: Add a CRC code to the sequence
function bits2ASCII(seq) #bits -> bytes (ASCII)
	two_power=[128 64 32 16 8 4 2 1] # 2^pos (starting from the left)
	n_seq=length(seq)
	bytes=[]
	for i=1:8:n_seq

		tmpByte=0
		p_j=1
		for j=i:i+7
			if(j<=n_seq)
				if(seq[j]==1 || seq[j]=='1' )
					tmpByte+=two_power[p_j]
				end
			else
				break
			end
			p_j+=1
		end
		push!(bytes,tmpByte)
	end
	return bytes
end

function CRC_Encode(seq)

	nbrCRCBits=[8,16,32] #CRC bits to add [8,16,32]
	CRC_len=0

	byte_seq=join(bits2ASCII(seq))
	nLen=length(byte_seq)

	if(nLen<100)
		CRC_len=nbrCRCBits[1]
	elseif(nLen<1000)
		CRC_len=nbrCRCBits[2]
	else
		CRC_len=nbrCRCBits[3]
	end

	#seq -> bytes
        crc::UInt32=0x00000000

	res_String_bits=string(crc32c(byte_seq,crc),base=2)
	
	return seq*res_String_bits[1:CRC_len]

	

end
