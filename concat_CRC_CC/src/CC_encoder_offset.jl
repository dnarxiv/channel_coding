#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#



struct state
       id::Array{Int64,1}		# current state [[00] [01] [10] [11]] (only one state)
       inputs::Array{Array{Int64,1},1}	# All possible inputs[0 1], for each input its own output located at the same index in outputs field
       outputs::Array{Array{Array{Int64,2},1},1}	# All possible outputs [[0 0] [1 1] ...] all possible outputs depending on inputs
       nextStates::Array{Array{Int64,1},1}		# All possible next states [[00] [01] [10] [11]] (go to the right one depending on input and currState)
end



function GenerateCmb(len,q) #generate allpossible combination for a vector of length l, and alphabet q

	Cmb=[] #all possiblecombinations
	symbolShot=trunc(Int,(q^len)/q) #number of same elements (will be updated at each column
	nbrStates=q^len

	for i1=1:len #L:constraint length
		i2=1
		while (i2<nbrStates)	#number of states
			for i3=0:q-1 #alternate between symbols
				for i4=1:symbolShot #swap shot[number of same symbols that will be used on one shot] ex: column1: 0 0 0 0 1 1 1 1, column2: 0 0 1 1 0 0 1 1, ... 
					if(i1>1)
						push!(Cmb[i2],i3)				
					else

						push!(Cmb,[i3])				
					end
					i2=i2+1
				end
			end
		end
		symbolShot=trunc(Int,symbolShot/(q))
	end

	return Cmb
end


#D:is a matrix that contains all generator polynomes
#states: vector of vectors containing all possible states
#L:constrained length
#k:inputLength
#q:alphabet length
function GenerateStateDiagram(states,D,L,k,q) #generate allpossible combination for a vector of length l, and alphabet q
	diagram=[] #will contain all possible states (state structure) & their details (transitions, inputs/outputs)


	inList=GenerateCmb(k,q) ##all possible inputs

	for i=1:length(states[:,1])
		s=copy(states[i,:]) 
		s=pop!(s) #pop is used to extract only one vector "[0,1,2]" instead of a vector in a vector "[[0,1,2]]"
		inputs=[]
		nextStates=[]
		outputs=[]
		for j=1:length(inList[:,1])
			input=copy(inList[j,:])
			input=pop!(input)

			push!(inputs,input)
			tmpOutputs=[]

			tmpIn=[inputs[j];s] #concat(merge) input & states to compute output  ??????????????????????
			
			push!(tmpOutputs,matrixGFMult(transpose(tmpIn),transpose(D)))
			push!(nextStates,tmpIn[1:end-k]) # shift 
			push!(outputs,tmpOutputs)
			
		end
		
		#create & provide state details &transitions 
		#println("input:",inputs)
		#println("output:",outputs)
		push!(diagram,state(s,inputs,outputs,nextStates))
		
   
	end

	return diagram
end

function findIndex(vect,Len,q)
	
	symbolShot=trunc(Int,(q^Len)/q) #number of same elements (will be updated at each column

	index=0
	for i1=1:Len #Len:constraint length(L) or any other length
		index=index+(vect[i1]*symbolShot)
		symbolShot=trunc(Int,symbolShot/(q))
	end

	return (index+1) #otherwise first index will be 0

end

function checks(stateDiagram,seq,k,L,q)
	nbrStates=length(stateDiagram)

	if(nbrStates<=0)
		println("Exit ERROR: Empty state diagram!")
		exit(1)
	end
	
	l=length(seq)
	resDiv=l/k
	if(resDiv!=trunc(Int,resDiv))
		println("Exit ERROR: Sequence to encode length should be a multiple of k=",k,"!")
		exit(1)
	end

end

function initCCEncoder(D,L,k,q,seq)
	states=GenerateCmb(L,q)

	stateDiagram=GenerateStateDiagram(states,D,L,k,q)  #stateIndex will be used to easily locate the current state entry on a vector

	checks(stateDiagram,seq,k,L,q)
	return  stateDiagram
end

function encodeFrame(stateDiagram,currState,input,L,q)
	
	inLen=length(stateDiagram[1].inputs[1])

	currStateIndex=findIndex(currState,L,q)
	inputIndex=findIndex(input,inLen,q)
	#println("Stateindex=",currStateIndex)
	#println("Inindex=",inputIndex)


	return stateDiagram[currStateIndex].outputs[inputIndex], stateDiagram[currStateIndex].nextStates[inputIndex]
end

function string2Vect(seq)
	len=length(seq)	
	seqVect=[]
	
	for i=1:len
		push!(seqVect,parse(Int,seq[i]))
	end

	return seqVect
end

function vect2Seq(vect)
	vect=pop!(copy(vect))
	len=length(vect)	
	seq=""

	for i=1:len	
		seq="$seq$(vect[i])"

	end

	return seq
end

function CCEncoder(stateDiagram,seq,k,L,q) #convolutional code encoder

	currState=stateDiagram[1].id #init to "000"
	len=length(seq)
	#string -> vector
	seqVect=string2Vect(seq)
	

	c="" #encoded sequence
	out="" #frame

	for i=1:k:len

		out,currState=encodeFrame(stateDiagram,currState,seqVect[i:i+(k-1)],L,q)

		out=vect2Seq(out)
		c="$c$out" #concat
	end

	#FLUSH THE ENCODER
	#*while (currState!=stateDiagram[1].id) #go back to initial state if not reached at the end
	bitTails=L #number of shifts before flushing the encoder
	while (bitTails>0) #go back to initial state if not reached at the end
		tmp=Int.(zeros(1,k))

		out,currState=encodeFrame(stateDiagram,currState,tmp,L,q)

		out=vect2Seq(out)
		c="$c$out" #concat

		bitTails-=1
	end
	
	return c
end

function addOffset(c)

	LenC=length(c)
	cOffset=""
	for i=1:LenC

		tmp=xor(parse(Int,c[i]),randSeq[i])
		cOffset="$cOffset$tmp"

	end
	return cOffset
end

#############################
#	MAIN ()
############################

### Right now just take as an argument $seq, later make it more dynamic (add k,m,q,D as arguments)
##CC_encoder.jl $seq 

function CCEncoder_Call(X)

	if(length(X)==0)
		println("error: Convolution Encoder, no input provided !")
		exit(1)
	end


	k=1; #input length
	m=3; # memory length (nbr of registers)
	n=2; #output length 
	q=2; #alphabet length
	L=k*(m-1) ; #constraint length
	nbrStates=q^L;


	seq=X


	D=[1 0 1; 1 1 1 ] #used by TUM ?


	stateDiagram=initCCEncoder(D,L,k,q,seq)

	c=CCEncoder(stateDiagram,seq,k,L,q)  #encoded code word

	#add offset
	#load variable "randSeq" which is a sequence of 10k binary simbols

	c=addOffset(c)


	#println(c)
	return c
end

###
#debug
###
#index=findIndex(stateDiagram[37].id,L,q)

#output=encodeFrame(stateDiagram,stateDiagram[37].id,stateDiagram[37].inputs[7],L,q)

#println("output=",output)

#for i=1:length(stateDiagram)
#	println("-----------")
#	println("state=",stateDiagram[i].id)
#	println("input=",stateDiagram[i].inputs)
#	println("outputs=",stateDiagram[i].outputs)
#	println("nextState=",stateDiagram[i].nextStates)

#end







