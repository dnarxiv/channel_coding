#!/bin/bash
#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

#----------------------------------------------------------------------------------------------#
# This script parses the file obtained after the consensus				       #
# It aims to extract the sequence with same length than the reference or 		       #
# the closest one (chose one before or after? {now,I choose the one after i.e lower than ref}) #
#----------------------------------------------------------------------------------------------#

#$1:file obtained after consensus
#$2:length of ref

prevDir=$(pwd)

cd $(dirname $0)
	

	outFile="$3"
	

	#capture number of lines
	nbrL=$(wc -l $1 | awk '{print $1}')
	((nbrL=nbrL/2)) #substract header lines ">"


	i=1
	next=1 # next line index

	while [ "$i" -le "$nbrL" ]
	do
	   pp="${next}p"  #header selection
	   
	   header=$(sed -n -e $pp $1)
	   
           #split header name (>consensus_342) to get the length (last part)

	     #replace "_" with "\n" (several lines)   #get the last line (length of the sequence)

	   len=$(sed  's/_/\n/1' <<< $header   |  tail -n 1) 
	   

		#the consensus output file is sorted by length descendigly 
	   if [ "$len" -le "$2" ] ; then    #select the one with same length or the one after 

    	        echo $header > $outFile

		((next=next+1))  #sequence selection
		pp="${next}p"     #sequence selection
	        seq=$(sed -n -e $pp $1)
		echo $seq >> $outFile
			
		echo "- 1 consensus seq of length $len selected  "
		break
	   fi
			
		#if no seq has the same length nor is less, take the last seq (lenght<)    
	   if [ "$i" -eq "$nbrL" ] ; then

		echo $header > $outFile

		((next=next+1))  #sequence selection
		pp="${next}p"     #sequence selection
	        seq=$(sed -n -e $pp $1)
		echo $seq >> $outFile
			
		echo "- 1 consensus seq of length $len selected  "		
		break
	   fi

	   ((next=next+2))
	   ((i++))   

	done


cd $prevDir
