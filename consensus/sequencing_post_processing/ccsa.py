import os
import sys
import networkx as nx
import random #to use random.shuffle(READS)

# compute the reverse complement of a DNA sequence                                                                                                    
TRANSTAB  = str.maketrans("ACGT", "TGCA")
def reverseComplement(seq):
    global TRANSTAB
    return seq.translate(TRANSTAB)[::-1]

def parseArg(arg,type,mo,val):
    find = False
    for i in range(len(sys.argv)):
        if sys.argv[i] == arg:
            find = True
            if type == "in_string":
                if os.path.exists(sys.argv[i+1]) == False:
                    print ()
                    print ("ERROR: cannot find read file:",sys.argv[i+1])
                    print ("exit")
                    print ()
                    sys.exit()
                else:
                    return sys.argv[i+1]
            elif type == "out_string":
                return sys.argv[i+1]
            elif type == "int":
                if sys.argv[i+1].isdigit() == False:
                    print ()
                    print ("ERROR: positive integer expected for argument",sys.argv[i])
                    print ("exit")
                    print ()
                    sys.exit()
                else:
                    return int(sys.argv[i+1])
    if mo == True and find == False:
        print ()
        print ("ERROR: argument",arg,"is missing")
        print ("exit")
        print ()
        sys.exit()
    if mo == False:
        return val
    return "null"
    

# ===========================================================================================
# START PROGRAM
# ===========================================================================================

# usage ccsa [arguments] {arguments2}
# arguments:
#    -read   <filename> 
#    -primer <filename> 
#    -length <integer> 
#    -out    <filename>
#    -graph  <filename>  -- optional
#    -n      <integer>   -- optional [default=100]
#    -kmax   <integer>   -- optional [default=20]
#    -kmin   <integer>   -- optional [default=9]
#    -help 
# arguments2 (not required):
#

print ()
print ("======================================================================")
print ("Constrained Consensus Sequence Algorithm       version 1.0 --  04/2020")
print ("D. Lavenier - CNRS/IRISA")
print ("======================================================================")
print ()

# check arguments
for i in range(len(sys.argv)):
    if sys.argv[i][0] == '-':
        if sys.argv[i] == "-help":
            print ("USAGE: python ccsa.py [arguments]")
            print ("       arguments:")
            print ("         -read    <filename>    [in]   read file (fastq format)")
            print ("         -primer  <filename>    [in]   surrounding sequences (fasta format)")
            print ("         -out     <filename>    [out]  consensus sequences output (fasta format)")
            print ("         -graph   <filename>    [out]  kmer overlap graph (gexf format) [optional]")
            print ("         -n       <integer>     [in]   maximum number of sequences to use during consensus [optional, default=100]")
            print ("         -kmax    <integer>     [in]   maximum size of a k-mer to consider during consensus [optional, default=20]")
            print ("         -kmin    <integer>     [in]   minimum size of a k-mer to consider during consensus [optional, default=9]")
            print ()
            sys.exit()
        if sys.argv[i] not in ["-read","-primer","-out","-length","-graph","-n","-kmax","-kmin"]:
            print ()
            print ("ERROR: unknown argument:",sys.argv[i])
            print ("exit...")
            print ()
            sys.exit()

# get arguments
# -------------
consensus_length = parseArg("-length","int",True,0)
read_filename    = parseArg("-read","in_string",True,"none")
primer_filename  = parseArg("-primer","in_string",True,"none")
out_filename     = parseArg("-out","out_string",True,"none")
graph_filename   = parseArg("-graph","out_string",False,"none")
NB_READ_MAX      = parseArg("-n","int",False,100)
KMER_SIZE_MAX    = parseArg("-kmax","int",False,20) #Introduce errors when changed
KMER_SIZE_MIN    = parseArg("-kmin","int",False,9)  #Introduce errors when changed

print ("read file name:     ",read_filename)
print ("surrounding seq:    ",primer_filename)
print ("consensus length:   ",consensus_length)
print ("number of reads:    ",NB_READ_MAX)
print ("Kmer size (max):    ",KMER_SIZE_MAX)
print ("Kmer size (max):    ",KMER_SIZE_MIN)

if graph_filename != "none":
    print ("graph filename:     ",graph_filename)

# define parameters
# -----------------
 #NB_READ_MAX      = 100
 #KMER_SIZE_MAX    = 20
 #KMER_SIZE_MIN    = 9


OVERLAP_MIN = {}
for i in range(KMER_SIZE_MIN,KMER_SIZE_MAX+1):
    OVERLAP_MIN[i] = {}

SOLID_THRESHOLD = {}
for i in range(KMER_SIZE_MIN,KMER_SIZE_MAX+1,1):
    SOLID_THRESHOLD[i] = {}


# 320 >= nb read >= 340  #add by me, discuss this with Domonique
for j in range(320,340):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 16
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 17
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 18
        OVERLAP_MIN[i][j] = 19
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 19
        OVERLAP_MIN[i][j] = 18



# 300 >= nb read >= 320  #add by me, discuss this with Domonique
for j in range(300,320):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 15
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 16
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 17
        OVERLAP_MIN[i][j] = 18
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 18
        OVERLAP_MIN[i][j] = 17



# 280 >= nb read >= 300  #add by me, discuss this with Domonique
for j in range(280,300):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 14
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 15
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 16
        OVERLAP_MIN[i][j] = 17
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 17
        OVERLAP_MIN[i][j] = 16


# 260 >= nb read >= 280  #add by me, discuss this with Domonique
for j in range(260,280):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 13
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 14
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 15
        OVERLAP_MIN[i][j] = 16
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 16
        OVERLAP_MIN[i][j] = 15


# 240 >= nb read >= 260  #add by me, discuss this with Domonique
for j in range(240,260):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 12
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 13
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 14
        OVERLAP_MIN[i][j] = 15
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 15
        OVERLAP_MIN[i][j] = 14



# 220 >= nb read >= 240  #add by me, discuss this with Domonique
for j in range(220,240):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 11
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 12
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 13
        OVERLAP_MIN[i][j] = 14
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 14
        OVERLAP_MIN[i][j] = 13

# 200 >= nb read >= 220  #add by me, discuss this with Domonique
for j in range(200,220):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 10
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 11
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 12
        OVERLAP_MIN[i][j] = 13
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 13
        OVERLAP_MIN[i][j] = 12

# 180 >= nb read >= 200  #add by me, discuss this with Domonique
for j in range(180,200):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 9
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 10
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 11
        OVERLAP_MIN[i][j] = 12
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 12
        OVERLAP_MIN[i][j] = 11

# 160 >= nb read >= 180  #add by me, discuss this with Domonique
for j in range(160,180):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 8
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 9
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 10
        OVERLAP_MIN[i][j] = 11
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 11
        OVERLAP_MIN[i][j] = 10


# 160 >= nb read >= 180  #add by me, discuss this with Domonique
for j in range(160,180):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 8
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 9
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 10
        OVERLAP_MIN[i][j] = 11
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 11
        OVERLAP_MIN[i][j] = 10


# 120 >= nb read >= 160  #add by me, discuss it with Dominique
for j in range(120,160):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 7
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 8
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 9
        OVERLAP_MIN[i][j] = 10
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 10
        OVERLAP_MIN[i][j] = 9


# 101 >= nb read >= 120  #add by me, discuss it with Dominique
for j in range(101,120):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 6
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 7
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 8
        OVERLAP_MIN[i][j] = 9
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 9
        OVERLAP_MIN[i][j] = 8


# 80 >= nb read >= 100
for j in range(80,101):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 5
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 6
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 7
        OVERLAP_MIN[i][j] = 8
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 8
        OVERLAP_MIN[i][j] = 7

# 60 >= nb read >= 79
for j in range(60,80):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 4
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 5
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 6
        OVERLAP_MIN[i][j] = 7
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 7
        OVERLAP_MIN[i][j] = 7

# 40 >= nb read >= 59
for j in range(40,60):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 3
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 4
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 5
        OVERLAP_MIN[i][j] = 7
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 6
        OVERLAP_MIN[i][j] = 7

# 30 >= nb read >= 39
for j in range(30,40):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = int(i/2)
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 3
        OVERLAP_MIN[i][j] = int(i/2)
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 4
        OVERLAP_MIN[i][j] = 7
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 5
        OVERLAP_MIN[i][j] = 6

# 20 >= nb read >= 29
for j in range(20,30):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = int(i/2)-2
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = int(i/2)-1
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 3
        OVERLAP_MIN[i][j] = 6
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 4
        OVERLAP_MIN[i][j] = 5

# 10 >= nb read >= 19
for j in range(10,20):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = int(i/2)-2
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = int(i/2)-1
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = 5
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 3
        OVERLAP_MIN[i][j] = 4

# 1 >= nb read >= 10
for j in range(1,10):
    # 18 >= kmer size >= 20  
    for i in range (18,21):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = int(i/2)-2
    # 15 >= kmer size >= 17
    for i in range (15,18):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = int(i/2)-1
    # 12 >= kmer size >= 14
    for i in range (12,15):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = 4
    # 9 >= kmer size >= 11
    for i in range (9,12):
        SOLID_THRESHOLD[i][j] = 2
        OVERLAP_MIN[i][j] = 3






# get surrounding sequences
# -------------------------
print ()
print ("Get surrounding sequences")
ff = open(primer_filename)
ff.readline()
LEFT_SEQ = ff.readline()[:-1]
ff.readline()
RIGHT_SEQ = ff.readline()[:-1]
print ("  - LEFT: ",LEFT_SEQ)
print ("  - RIGHT:",RIGHT_SEQ)
ff.close()


# Select reads
# ------------
print ()
min_len = len(LEFT_SEQ) + len(RIGHT_SEQ) + consensus_length
print ("Select reads >",consensus_length,"bp") 
READS = []
ff = open(read_filename)
nb_read_tot = 0 #nbr of reads
nb_read = 0     #nbr of reads length >consensus length
com = ff.readline()
while com != "":
    seq = ff.readline()[:-1]
    ff.readline()
    ff.readline()
    if len(seq) >= consensus_length:
        READS.append(seq)
        nb_read += 1
    nb_read_tot += 1
    com = ff.readline()
ff.close()
print ("  - select",nb_read,"reads over",nb_read_tot)

random.shuffle(READS) # shuffle selected reads, this way selected reads (based on surrounding sequences) are selected randomly from reads (based on length) during the next step  
#

# Select reads based on surrounding sequences and reference
# --------------------------------------------------------
print ()
print ("Select reads based on surrounding sequences")
# index LEFT and RIGHT surrounding sequences
kmer_size = 8
LEFT_KMERS = {}
for i in range(len(LEFT_SEQ)-kmer_size+1):
    kmer = LEFT_SEQ[i:i+kmer_size]
    LEFT_KMERS[kmer] = i
RIGHT_KMERS = {}
for i in range(len(RIGHT_SEQ)-kmer_size+1):
    kmer = RIGHT_SEQ[i:i+kmer_size]
    RIGHT_KMERS[kmer] = i + kmer_size
SEL_READS = []
# consider all reads and their complement
for read in READS:
    for rd in [read,reverseComplement(read)]:
        ns = 0
        for i in range(int(len(rd)/2)):
            kmer = rd[i:i+kmer_size]
            if kmer in LEFT_KMERS:
                start = max(0,i-LEFT_KMERS[kmer])
                ns += 1
                break
        if ns >= 1:
            ne = 0
            li = min(start + int(consensus_length*1.2) + len(LEFT_SEQ)+len(RIGHT_SEQ),len(rd))
            for i in range(li-kmer_size,int(li/2),-1):
                kmer = rd[i:i+kmer_size]
                if kmer in RIGHT_KMERS:
                    end = i+RIGHT_KMERS[kmer]
                    ne += 1
                    break
            if ne >= 1 and end-start >= consensus_length and len(SEL_READS) < NB_READ_MAX :
                SEL_READS.append(rd[start:end])
                break
print ("  - get",len(SEL_READS),"reads")

# Build Kmer Overlap Graph
# ------------------------

# extractSolidKmers
# -----------------
# input:
#  SR               : selected reads - type = list
#  LEFT_SEQ         : left marker (~40bp) - type = string
#  RIGHT_SEQ        : right maker (~40bp) - type = string
#  kmer_size        : size of the kmer - type = integer
#  consensus_length : expected length of the consensus sequence - type = integer
#  solid_threshold  : number of kmers to consider a kmer as solid - type = integer

def extractSolidKmers(SR,LEFT_SEQ,RIGHT_SEQ,kmer_size,consensus_length,solid_threshold):
    # count kmers
    # CPT_KMER[kmer] = [i1, i2, ... in] *** i = position of the kmer in the different reads
    # position is normalized
    CPT_KMER = {} # Kmer counter
    for read in SR:
        l = len(LEFT_SEQ)+len(RIGHT_SEQ)+consensus_length
        for i in range(len(read)-kmer_size+1):
            kmer = read[i:i+kmer_size]
            if kmer not in CPT_KMER:
                CPT_KMER[kmer] = [int(i*(l/len(read)))] # compute a pseudo normalized position
            else:
                CPT_KMER[kmer].append(int(i*(l/len(read))))
    # SOLID_KMERS[kmer] = [ [kmer_14, min_pos, max_pos, nb_kmer], [kmer_354, ,min_pos, max_pos, nb_kmer], ... ]
    SOLID_KMERS = {}
    for kmer in CPT_KMER:
        if len(CPT_KMER[kmer]) >= solid_threshold: # consider solid kmers only
            CPT_KMER[kmer].sort() # sort positions of the kmer
            SOLID_KMERS[kmer] = []
            # group similar positions together
            l = [CPT_KMER[kmer][0]]
            for i in range(1,len(CPT_KMER[kmer])):
                if CPT_KMER[kmer][i] - CPT_KMER[kmer][i-1] < 20 : # max distance between 2 positions
                    l.append(CPT_KMER[kmer][i])
                else:
                    if len(l) >= solid_threshold:
                        SOLID_KMERS[kmer].append(l)
                    l = [CPT_KMER[kmer][i]]

            if len(l) >= solid_threshold:
                SOLID_KMERS[kmer].append(l)
            if len(SOLID_KMERS[kmer]) == 0:
                del SOLID_KMERS[kmer]
            else:
                # name kmer according to its position
                for n in range(len(SOLID_KMERS[kmer])):
                    name_kmer = kmer+"_"+str(SOLID_KMERS[kmer][n][0])
                    SOLID_KMERS[kmer][n] = [name_kmer,SOLID_KMERS[kmer][n][0],SOLID_KMERS[kmer][n][len(SOLID_KMERS[kmer][n])-1],len(SOLID_KMERS[kmer][n])]
    # determine start and end kmers
    START_KMER = ""
    for i in range(len(LEFT_SEQ)-kmer_size,0,-1):
        kmer = LEFT_SEQ[i:i+kmer_size]
        if kmer in SOLID_KMERS:
            START_KMER = SOLID_KMERS[kmer][0][0]
            break
    END_KMER = ""
    for i in range(len(RIGHT_SEQ)-kmer_size):
        kmer = RIGHT_SEQ[i:i+kmer_size]
        if kmer in SOLID_KMERS:
            END_KMER = SOLID_KMERS[kmer][0][0]
            break
    # return solid kmers + start/end kmers
    return SOLID_KMERS, START_KMER, END_KMER


def hasCycle(G,S,level,cn,n):
    if level > 20 :
        return False
    if S[cn] == n:
        return False
    C = False
    for p in G.predecessors(cn):
        if p == n:
            C = True
        else:
            C = C or hasCycle(G,S,level+1,p,n)
        if C == True:
            break
    S[cn] = n
    return C

def existCycle(G):
    S = {}
    for n in G:
        S[n] = "none"
    for n in G:
        S[n] = n
        C = False
        for p in G.predecessors(n):
            if p == n:
                return True
            else:
                C = C or hasCycle(G,S,0,p,n)
            if C == True:
                return True
    return False

print ()
print ("Build Kmer Overlap Graph (KOG)")

kmer_size = KMER_SIZE_MAX
print ("    len(kmer)    Start                      End")
while True: 
    # compute solid kmers
    SOLID_KMERS, START_KMER, END_KMER = extractSolidKmers(SEL_READS,LEFT_SEQ,RIGHT_SEQ,kmer_size,consensus_length,SOLID_THRESHOLD[kmer_size][len(SEL_READS)])
    print (str(kmer_size).rjust(13),"  ",START_KMER.ljust(26),END_KMER)
    IOV = kmer_size - OVERLAP_MIN[kmer_size][len(SEL_READS)]
    # build graph
    GRAPH = nx.DiGraph()
    x = len(LEFT_SEQ) + len(RIGHT_SEQ) + consensus_length
    for read in SEL_READS:
        for i in range(len(read)-kmer_size+1):
            ii = int(i*(x/len(read)))
            kmer1 = read[i:i+kmer_size]
            if kmer1 in SOLID_KMERS:
                node_name1 = "none"
                for l1 in SOLID_KMERS[kmer1]:   # l = [kmer_x, min_pos, max_pos, nb_kmer]
                    if ii >= l1[1] and ii<= l1[2]:
                        node_name1 = l1[0]
                        break
                if node_name1 != "none":
                    for k in range(1,IOV):
                        kk = int((i+k)*(x/len(read)))
                        kmer2 = read[i+k:i+k+kmer_size]
                        if kmer2 in SOLID_KMERS:
                            node_name2 = "none"
                            for l2 in SOLID_KMERS[kmer2]:
                                if kk >= l2[1] and kk <= l2[2]:
                                    node_name2 = l2[0]
                                    break
                            if node_name2 != "none" and node_name1 != node_name2:
                                if node_name1  not in GRAPH:
                                    if node_name1 == START_KMER:
                                        GRAPH.add_node(node_name1,type='S',w=l1[3])
                                    elif node_name1 == END_KMER:
                                        GRAPH.add_node(node_name1,type='E',w=l1[3])
                                    else:
                                        GRAPH.add_node(node_name1,type='X',w=l1[3])
                                if node_name2  not in GRAPH:
                                    if node_name2 == START_KMER:
                                        GRAPH.add_node(node_name2,type='S',w=l2[3])
                                    elif node_name2 == END_KMER:
                                        GRAPH.add_node(node_name2,type='E',w=l2[3])
                                    else:
                                        GRAPH.add_node(node_name2,type='X',w=l2[3])
                                GRAPH.add_edge(node_name1,node_name2,weight=k)

    G = GRAPH.to_undirected()
    GC = max((G.subgraph(c) for c in nx.connected_components(G)), key=len)
    if START_KMER in GC and END_KMER in GC:
        list_nodes = list(GRAPH.nodes)
        for n in list_nodes:
            if n not in GC:
                GRAPH.remove_node(n)
        if existCycle(GRAPH) == False:
            break

    kmer_size -= 1
    if kmer_size < KMER_SIZE_MIN:
        print ("  - cannot build KOG")
        print ()
        sys.exit()
    

# write graph
if graph_filename != "none":
    nx.write_gexf(GRAPH,graph_filename)

# Search paths in KOG
# -------------------

def searchMaxPath(node_name,end_name):
    global PATH_SCORE # PATH_SCORE[node][len] = [score,path]
    global STATUS     # free, done
    global GRAPH      # node = KMER_num
    global WEIGHT     # int
    if STATUS[node_name] == "done":
        return 
    if node_name == end_name:
        path = node_name.split('_')[0]
        score = WEIGHT[node_name]
        PATH_SCORE[node_name][len(path)] = [score,path]
        STATUS[node_name] = "done"
        return 
    for node in GRAPH.predecessors(node_name):
        edge_score = GRAPH[node][node_name]['weight']
        searchMaxPath(node,end_name)
        for l in PATH_SCORE[node] :
            score = PATH_SCORE[node][l][0] + WEIGHT[node_name]*edge_score
            path  = PATH_SCORE[node][l][1] + node_name.split('_')[0][-edge_score:]
            lp = len(path)
            if lp in PATH_SCORE[node_name]:
                if score > PATH_SCORE[node_name][lp][0] :
                    PATH_SCORE[node_name][lp] = [score,path]
            else:
                PATH_SCORE[node_name][lp] = [score,path]
    STATUS[node_name] = "done"
    return 

print ()
print ("Search paths in KOG")

STATUS = {}
PATH_SCORE   = {}
WEIGHT = dict(GRAPH.nodes(data='w'))
for node in GRAPH:
    STATUS[node] = "free"
    PATH_SCORE[node] = {}

# get the best path of various length between the start and end kmers
searchMaxPath(END_KMER,START_KMER)

# trim the sequences according to the START and END kmers
kmer_size = len(START_KMER.split('_')[0])
for i in range(len(LEFT_SEQ)-kmer_size,0,-1):
    kmer = LEFT_SEQ[i:i+kmer_size]
    if kmer == START_KMER.split('_')[0]:
        left_trim = len(LEFT_SEQ)-i
        break
for i in range(len(RIGHT_SEQ)-kmer_size):
    kmer = RIGHT_SEQ[i:i+kmer_size]
    if kmer == END_KMER.split('_')[0]:
        right_trim = i + kmer_size
        break

RES = []
for l in PATH_SCORE[END_KMER]:
    sequence = PATH_SCORE[END_KMER][l][1][left_trim:-right_trim]
    RES.append([len(sequence),sequence])

if len(RES) == 0:
    print ("  - no path found")
    print ()
    sys.exit()

RES.sort(key=lambda x: x[0], reverse=True)
print ("  - found",len(RES),"paths of length:",end=" ")
ff = open(out_filename,"w")
for i in range(len(RES)):
    ff.write(">consensus_"+str(RES[i][0])+"\n"+RES[i][1]+"\n")
    print (RES[i][0],end=" ")
print()
ff.close()

print()

