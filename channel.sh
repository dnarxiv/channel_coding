#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

#echo "-------------------------------------------"
#echo "|  DNA DATA STORAGE CHANNEL (SIMULATOR)   |"
#echo "-------------------------------------------"


	####
	#->INPUTS:
	#	-$1:Seq to simulate
	#	-$2:Nbr of reads to simulate
	#	-$3:output_path (EXTENSION SHOULD BE IN .fastq !!!)
	#	-$4:Channel memory length Fixed to k=6 [RECOMENDED TO NOT CHANGING IT] 
	#->OUTPUTS:
	#	-output_path: Simulated fastq
	###

prevDir=$(pwd)

cd $(dirname $0)
	#--
	#Check variables
	inPath=""
	outPath=""
	nbrRead=""
	k=""
	while [ "$1" != "" ]; do
    	case $1 in
		-i )
		    shift
		    inPath=$1  
		;;   
		-o )
		    shift
		    outPath=$1  
		;;            
		-n )    
		    shift
		    nbrRead=$1
		;;
		-k  )   
		    shift
		    k=$1
		;;

	    esac
	    shift
	done

	if [ -f "$prevDir/$inPath" ]
	then
		inPath=$prevDir/$inPath
	elif [ -f "$1" ]
	then
		inPath=$inPath
	else
		echo "error: input path is incorrect"
		exit 1
	fi


	if ! touch $prevDir/$outPath > /dev/null 2>&1 
	then
		if ! touch $outPath > /dev/null 2>&1 
		then
			echo "error:output path incorrect "
			exit 1
		else
			outPath=$outPath
		fi

	else
		outPath=$prevDir/$outPath
	fi

	if [ "$k" == "" ]
	then
		k=6
	else
		int='^[1-9][0-9]*$'
		if ! [[ $k =~ $int ]]
		then
		   echo "error: -k [1:10] (pick up a value between 1 and 10), [k=6 is recommended]"
		   exit 1
		elif [[ $k -gt "10" ]]
		then
		   echo "error: -k [1:10] (pick up a value between 1 and 10), [k=6 is recommended]"
		   exit 1
		fi
	fi

	

	int='^[0-9]+$'
	if ! [[ $nbrRead =~ $int ]] || [ $nbrRead -eq "0" ]
	then
		echo "error: value of 'n' should be greater than 0 -n [1:N]"
		exit 1
	
	fi

	#--
	#Check variables end		



	seq=$(sed -n -e 2p $inPath)
	rm -r -f 


	cd ./ourSimulator

		./channelEdit_chained_beg_end_YiKmerDependOnPrevYi.sh $seq $nbrRead $outPath $k 

	cd ..


cd $prevDir
