#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#


#echo "-------------------------------------------"
#echo "|  Channel_Encoder: NB-LDPC GF(4)          |"
#echo "-------------------------------------------"

		####
		#->INPUTS:
		#	-$1:SEQUENCE REF PATH, sequence should be of length =kInf, otherwise will be cut
		#	-$3:PRIMERS, file should respect a specific structure as in defaultPrimers.fasta file
		#	-$2:output_path 
		#->OUTPUTS:
		#	-output_path:ENCODED SEQUENCE
		###

prevDir=$(pwd)

cd $(dirname $0)

	#--
	#Check variables
		inPath=""
		outPath=""
		primersPath=""
		G_Matrix=""

		while [ "$1" != "" ]; do
	    	case $1 in
			-i )
			    shift
			    inPath=$1  
			;;   
			-o )
			    shift
			    outPath=$1  
			;;            
			-p )    
			    shift
			    primersPath=$1
			;;
			-G  )   
			    shift
			    G_Matrix=$1
			;;

		    esac
		    shift
		done

		if [ -f "$prevDir/$inPath" ]
		then
			inPath=$prevDir/$inPath
		elif [ -f "$inPath" ]
		then
			inPath=$inPath
		else
			echo "error: input path is incorrect"
			exit 1
		fi


		if [ -f "$prevDir/$primersPath" ]
		then
			primersPath=$prevDir/$primersPath
		elif [ -f "$primersPath" ]
		then
			primersPath=$primersPath
		else
			echo "error: primers path is incorrect"
			exit 1
		fi


		if ! touch $prevDir/$outPath > /dev/null 2>&1 
		then
			if ! touch $outPath > /dev/null 2>&1 
			then
				echo "error:output path incorrect "
				exit 1
			else
				outPath=$outPath
			fi

		else
			outPath=$prevDir/$outPath
		fi



		if [ -f "$prevDir/$G_Matrix" ]
		then
			G_Matrix=$prevDir/$G_Matrix
		elif [ -f "$G_Matrix" ]
		then
			G_Matrix=$G_Matrix
		else
			echo "error: G matrix path is incorrect"
			exit 1
		fi

	#Check variables end
	#----

		
		#LDPC Folder
		cd ./LDPC
			header=$(sed -n -e 1p $inPath)	
			seq=$(sed -n -e 2p $inPath)
			
			rm -f $output #erase previous data if exist


			#TAKE ONLY NECESSARY AMOUNT OF INFORMATION (if seq too long cut) 
			echo "$seq" > ./tmpSeq.txt 

			#encoder							
			c=$(julia NB_LDPC_encoder_DNA.jl ./tmpSeq.txt  $G_Matrix)

			rm -f ./tmpSeq.txt 

			#addBarcodes	of same length (constrained code)		
			pLeft=$(sed -n -e 2p $primersPath)
		        pRight=$(sed -n -e 4p $primersPath)
	

			echo $header > $outPath #fasta header
			#PRIMERS are added
			echo $pLeft$c$pRight >> $outPath #will contain the codeword (the sequence after encoding and primers add)
			
		cd ..


		#*echo "......Finished!"
		#*echo ""

cd $prevDir
