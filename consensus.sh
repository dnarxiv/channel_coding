#!/bin/bash

#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#	

	#-----------------
	#	Consensus
	#−-------------------

	####
	#->INPUTS:
	#	-$1:expected length
	#	-$2:simulated fastq
	#	-$3:primers files (left & right) in fasta format
	#	-$4:output_path reconstructed consensus 
	#	-$5:'m' number of sequences taken by the consensus, fixed to m=100
	#->OUTPUTS:
	#	-output_path : reconstructed consensus 
	###

prevDir=$(pwd)

cd $(dirname $0)
	
cd ./consensus
   #--
   #Check variables
	inPath=""
	outPath=""
	primersPath=""
	refLen=""
	m=""

	while [ "$1" != "" ]; do
    	case $1 in
		-i )
		    shift
		    inPath=$1  
		;;   
		-o )
		    shift
		    outPath=$1  
		;;
		-l )    
		    shift
		    refLen=$1
		;;            
		-p )    
		    shift
		    primersPath=$1
		;;
		-m  )   
		    shift
		    m=$1
		;;

	    esac
	    shift
	done

	if [ -f "$prevDir/$inPath" ]
	then
		inPath=$prevDir/$inPath
	elif [ -f "$inPath" ]
	then
		inPath=$inPath
	else
		echo "error: input path is incorrect"
		exit 1
	fi


	int='^[1-9]+[0-9]*$'
	if ! [[ $refLen =~ $int ]]
	then
	   echo "error: -l value  should be greater than 0"
	   exit 1
	fi

	if ! touch $prevDir/$outPath > /dev/null 2>&1 
	then
		if ! touch $outPath > /dev/null 2>&1 
		then
			echo "error:output path incorrect "
			exit 1
		else
			outPath=$outPath
		fi

	else
		outPath=$prevDir/$outPath
	fi
	
	if [ -f "$prevDir/$primersPath" ]
	then
		primersPath=$prevDir/$primersPath
	elif [ -f "$primersPath" ]
	then
		primersPath=$primersPath
	else
		echo "error: primers path is incorrect"
		exit 1
	fi


	if [ "$m" == "" ]
	then
		m=100
	else
		int='^[1-9][0-9]*$'
		if ! [[ $m =~ $int ]]
		then
		   echo "error: -m should be an integer value greater than 0"
		   exit 1
		fi
	fi


  #--
  #Check variables  end


	cF="sequencing_post_processing" #consensus folder
	consList=consList.fasta #save all possible consensus in this file before choosing the best (closest one in terms of length)
	rm -f $cF/*.fasta
	# rm consensuslog.txt #UNCOMMENT OR ADD (SOMEWHERE) THIS INSTRUCTIONS IF THIS MODULE IS USED ALONE : to avoid big concatenated log

	#count number of fastq and concatenate them if more than 1
	nbrFastq=$(ls $inPath | wc -l )
	

	
	if [ "$nbrFastq" -lt "1" ]
	then
		"Consensus.sh error! NO FASTQ DETECTED (check extensions)"
	fi
	
	


	
	python3 ./$cF/ccsa.py -read $inPath -primer $primersPath -length $refLen -out $cF/$consList -n $m -kmax 20 -kmin 9 >> consensuslog.txt 2>&1


	if [ -f "$cF/$consList" ]
	then

		./parseConsOut.sh $cF/$consList $refLen $outPath >> consensuslog.txt  2>&1
		
		echo "1"

	else
		echo "0" #Consensus: no reconstruction!
	fi

	
cd ..

cd $prevDir

