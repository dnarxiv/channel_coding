#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#


	#-------------------------------------------------
	#Decoding Scheme
	#−------------------------------------------------

	####
	#->INPUTS:
	#	-$1:expected length
	#	-$2:simulated fastq
	#	-$3:primers files (left & right) in fasta format
	#	-$4:output_path decoded sequence
	#	-$5:1->use, 0->don't use, undo avoid HP of max len>max module.
	#	-$6:'m' number of sequences taken by the consensus, fixed to m=100 
	#->OUTPUTS:
	#	-output_path : decoded sequence
	#	-print: 1-> the decoded sequence is correct, 0-> the decoded sequence is incorrect
	###


prevDir=$(pwd)

cd $(dirname $0)

   #--
   #Check variables
	inPath=""
	outPath=""
	primersFile=""
	refLen=""
	m=""
	undoAvoidLongHP=""
	H_Matrix=""

	while [ "$1" != "" ]; do
    	case $1 in
		-i )
		    shift
		    inPath=$1  
		;;   
		-o )
		    shift
		    outPath=$1  
		;;
		-l )    
		    shift
		    refLen=$1
		;;            
		-p )    
		    shift
		    primersPath=$1
		;;
		-m  )   
		    shift
		    m=$1
		;;
		-avoidLongHP )   
		    shift
		    undoAvoidLongHP=$1
		;;
		-H )   
		    shift
		    H_Matrix=$1
		;;

	    esac
	    shift
	done


	if [ -f "$prevDir/$inPath" ]
	then
		inPath=$prevDir/$inPath
	elif [ -f "$inPath" ]
	then
		inPath=$inPath
	else
		echo "error: input path is incorrect"
		exit 1
	fi


	int='^[1-9]+[0-9]*$'
	if ! [[ $refLen =~ $int ]]
	then
	   echo "error: -l value  should be greater than 0"
	   exit 1
	fi

	if ! touch $prevDir/$outPath > /dev/null 2>&1 
	then
		if ! touch $outPath > /dev/null 2>&1 
		then
			echo "error:output path incorrect "
			exit 1
		else
			outPath=$outPath
		fi

	else
		outPath=$prevDir/$outPath
	fi
	
	if [ -f "$prevDir/$primersPath" ]
	then
		primersPath=$prevDir/$primersPath
	elif [ -f "$primersPath" ]
	then
		primersPath=$primersPath
	else
		echo "error: primers path is incorrect"
		exit 1
	fi


	if [ "$m" == "" ]
	then
		m=100
	else
		int='^[1-9][0-9]*$'
		if ! [[ $m =~ $int ]]
		then
		   echo "error: -m should be an integer value greater than 0"
		   exit 1
		fi
	fi

	if [ "$undoAvoidLongHP" == "" ] || [ "$undoAvoidLongHP" == "0" ]
	then
		undoAvoidLongHP=0
	elif [ "$undoAvoidLongHP" == "1" ]
	then
		undoAvoidLongHP=1

	else
		echo "error: -avoidLongHP values incorrect! put 0:to disable avoid longHP decoder or 1: to enable it"
		exit 1

	fi

	if [ -f "$prevDir/$H_Matrix" ]
	then
		H_Matrix=$prevDir/$H_Matrix
	elif [ -f "$H_Matrix" ]
	then
		H_Matrix=$H_Matrix
	else
		echo "error: H matrix path is incorrect"
		exit 1
	fi

   #--
   #Check variables END


iMax=100 #number of consensus attempts until Imax reached or correct word decoded ($cvalid=1)


#*echo "-->m=$m"

i=0
cValid=0 #let 1 execution at least
consensusExist=0

rm  -f ./consensus_errCounting_chain/Consensuslog.txt #remove log

while [ "$i" -lt "$iMax" ] && [ $cValid -eq "0" ]
do

	cValid=0
	
	consensusExist=$(./consensus.sh -l $refLen -i $inPath -p $primersPath -o ${outPath}.tmp -m $m )
	
	if [ "$consensusExist" -eq "1" ]
	then
	

		#Undo avoid longHP encoding?
		#*if [ "$undoAvoidLongHP" -eq "1" ]
		#*then

		#*fi


		#USE SYNCHRONIZATION (if necessary) and NB-LDPC BP Decoder
		cValid=$(./channel_decoder.sh -i ${outPath}.tmp -l $refLen -o $outPath -H $H_Matrix )

		rm -f  ${outPath}.tmp 
		
	fi
	((i=$i+1))

done

echo $cValid 


cd $prevDir
