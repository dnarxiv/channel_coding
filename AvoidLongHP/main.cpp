#include <iostream>
#include <string>

#include <vector>
#include <cmath>
#include <fstream>
#include <bitset>
#include <cstdlib>

//--------------------------------------------------------------------------#
//	@Authors: Belaid Hamoum, Elsa Dupraz				    				#
//	Project : DnarXiv, funded by CominLabs				    				#
//	email   : belaid.hamoum@gmail.com				    					#
//--------------------------------------------------------------------------#


//g++ -std=c++11 main.cpp -o noLongHP3

void quatToBase(std::string const& in,std::string &out)
{
	int lenLine=in.size();

	for(int i=0;i<lenLine;i++)
	{
		if(in[i] == '0')
		{
			out.push_back('A');
		}
		else if (in[i] == '1')
		{
			out.push_back('C');
		}
		else if (in[i] == '2')
		{
			out.push_back('G');
		}
		else if (in[i] == '3')
		{
			out.push_back('T');
		}
		else
		{
			std::cout<<"error!"<<std::endl;
		}
	}
}

void baseToQuat(std::string const& in,std::string &out)
{
	int const lenLine=in.size();

	for(int i=0;i<lenLine;i++)  //penser aux maps
	{
		if(in[i] == 'A')
		{
			out.push_back('0');
		}
		else if (in[i] == 'C')
		{
			out.push_back('1');
		}
		else if (in[i] == 'G')
		{
			out.push_back('2');
		}
		else if (in[i] == 'T')
		{
			out.push_back('3');
		}
		else
		{
			std::cout<<"erreur_baseToQuat!"<<std::endl;
		}
	}
}


void binToQuat(std::string const& in,std::string &out)
{
	int lenLine=in.size()-1;
	int quater;

	for(int i=0; i<lenLine; i+=2)
	{
		quater = ((in[i]-48)<<1) + in[i+1]-48;

		out.push_back(quater+'0');
	}
}


void tritToBin(std::string & in,std::string &out)
{
	std::string tmpLine,strDec;
	std::vector<char> ternaire;
	int dec(0),tmp(0);
	bool last(false);

	std::string lastTwoTrits="";
	int nbrZerosBefore=0;
	lastTwoTrits.push_back(in.back());
	in.pop_back();
	lastTwoTrits.push_back(in.back());
	in.pop_back();

	if(lastTwoTrits=="00")
	{
		nbrZerosBefore=0;
	}
	else if (lastTwoTrits=="01")
	{
		nbrZerosBefore=3;
	}
	else if (lastTwoTrits=="02")
	{
		nbrZerosBefore=6;
	}
	else if (lastTwoTrits=="10")
	{
		nbrZerosBefore=1;
	}
	else if (lastTwoTrits=="11")
	{
		nbrZerosBefore=4;
	}
	else if (lastTwoTrits=="12")
	{
		nbrZerosBefore=7;
	}
	else if (lastTwoTrits=="20")
	{
		nbrZerosBefore=2;
	}
	else if (lastTwoTrits=="21")
	{
		nbrZerosBefore=5;
	}
	else if (lastTwoTrits=="22")
	{
		nbrZerosBefore=8;
	}

	int lenLine=in.size();
	for(int i=0; i<lenLine; i+=6)
	{

		if(i+6>=lenLine)
		last=true;

		if(lenLine-i>=6 && tmp==0)
		{
			for(int j=0;j<6;j++)
			{
				dec+= ((in[j+i]-48)*(std::pow(3,5-tmp))) ;
				tmp++;
			}
		}
		else
		{
			if(lenLine-i<6)
			{
				for(int j=0;j+i<lenLine;j++)
				{
					dec+= ((in[j+i]-48)*(std::pow(3,lenLine-i-1-tmp))) ;
					tmp++;
				}
			}
			else
			{
				int m=6-tmp;
				for(int j=0;j<m;j++)
				{
					dec+= ((in[j+i]-48)*(std::pow(3,5-tmp))) ;
					tmp++;
				}
				i=i-6+m;
			}
		}

		if(tmp==6)
		{
			strDec=std::bitset<8>(dec).to_string();
			out=out+strDec;
			dec=0;
			tmp=0;
			strDec="";
		}
		if(last)
		{
			strDec=std::bitset<8>(dec).to_string();

			for(int ii=0;ii<strDec.size();ii++)
			{

				if(strDec[ii]!='1')
				{
					if(nbrZerosBefore>0)
					{
						nbrZerosBefore--;
					}
					else
					{
						strDec.erase(ii,1);
						ii--;
					}
				}
				else
				{
					break;
				}
			}

			int lTmp=out.length();

			for(int ii=lTmp;ii<nbrZerosBefore;ii++)
			{
				strDec.insert(0,1,'0');
			}
			out=out+strDec;
		}
	}
}


void binToTrit(std::string const& in,std::string &out)
{
	std::vector<int> ternaire;
	int lenLine=in.size();
	int nbrTer,dec;
	bool okay(false),last(false);
	std::string lastTwoTrits="" ;

	for(int i=0; i<lenLine; i+=8)
	{
		okay=false;
		dec=0;

		for(int j=0;j<8;j++)
		{
			if(i+8<lenLine)
			{
				dec+= ((in[j+i]-48)<<7-j) ;
			}
			else
			{
				if(i+j<lenLine)
				{
					dec+= ((in[j+i]-48)<<(lenLine-i-1)-j) ;
				}
			}
		}
		if(i+8>=lenLine)
			last=true;

		while(!okay)
		{
			ternaire.push_back(dec%3);
			dec=(int)dec/3;

			if(dec<3)
			{
				ternaire.push_back(dec);
				okay=true;
			}
		}

		nbrTer=ternaire.size();
		if(nbrTer<6)
		{
			if(!last)
			{
				for (int i=0;i<6-nbrTer;i++)
					ternaire.push_back(0);
			}
		}
		if(last)
		{
			int nbrZerosBefore=0;
			for(int j=0;j<8;j++)
			{
				if(i+j<lenLine && in[j+i]=='0' )
				{
					nbrZerosBefore++;
				}
				else
				{
					break;
				}
			}
			if (nbrZerosBefore==0)
			{
				lastTwoTrits="00";
			}
			else if (nbrZerosBefore==1)
			{
				lastTwoTrits="01";
			}
			else if (nbrZerosBefore==2)
			{
				lastTwoTrits="02";
			}
			else if (nbrZerosBefore==3)
			{
				lastTwoTrits="10";
			}
			else if (nbrZerosBefore==4)
			{
				lastTwoTrits="11";
			}
			else if (nbrZerosBefore==5)
			{
				lastTwoTrits="12";
			}
			else if (nbrZerosBefore==6)
			{
				lastTwoTrits="20";
			}
			else if (nbrZerosBefore==7)
			{
				lastTwoTrits="21";
			}
			else if (nbrZerosBefore==8)
			{
				lastTwoTrits="22";
			}
		}
		while(!ternaire.empty())
		{
			out.push_back(ternaire.back()+'0');
			ternaire.pop_back();
		}
		if(last)
		{
			out.push_back(lastTwoTrits[0]);
			out.push_back(lastTwoTrits[1]);
		}
	}
}


bool quatToBin(std::string const& in,std::string &out)
{
	std::string tmpLine,tmpStr("");
	int lenLine=in.size();

	tmpStr="";

	for(int i=0; i<lenLine; i++)
	{

		if(in[i]==48)       // 48='0'
		{
			tmpStr+="00";
		}
		else if(in[i]==49)   // 49='1'
		{
			tmpStr+="01";
		}
		else if(in[i]==50)   // 50='2'
		{
			tmpStr+="10";
		}
		else if(in[i]==51)   // 51='3'
		{
			tmpStr+="11";
		}
		else
		{
			std::cout<<"erreur!"<<std::endl;
			return false;
		}
	}
	out=tmpStr;

	return true;
}


std::string encode(std::string s)
{
	int rep=3;
	char lastBase='A';
	std::string res="";

	for(int i=0,n=s.length();i<n;i++)
	{
		switch(s[i])
		{
			case '0':
				switch(lastBase)
				{
					case 'A':
						if(rep<3)
						{
							res.push_back('A');
							rep++;
						}
						else
						{
							res.push_back('C');
							rep=1;
							lastBase='C';
						}
						break;
					case 'C':
						if(rep<3)
						{
							res.push_back('C');
							rep++;
						}
						else
						{
							res.push_back('G');
							rep=1;
							lastBase='G';
						}
						break;
					case 'G':
						if(rep<3)
						{
							res.push_back('G');
							rep++;
						}
						else
						{
							res.push_back('T');
							rep=1;
							lastBase='T';
						}
						break;
					case 'T':
						if(rep<3)
						{
							res.push_back('T');
							rep++;
						}
						else
						{
							res.push_back('A');
							rep=1;
							lastBase='A';
						}
						break;
				}
				break;

			case '1':
                rep=1;
				switch(lastBase)
				{
					case 'A':
						res.push_back('G');
						lastBase='G';
						break;
					case 'C':
						res.push_back('T');
						lastBase='T';
						break;
					case 'G':
						res.push_back('A');
						lastBase='A';
						break;
					case 'T':
						res.push_back('C');
						lastBase='C';
						break;
				}
				break;

			case '2':
                rep=1;
				switch(lastBase)
				{
					case 'A':
						res.push_back('T');
						lastBase='T';
						break;
					case 'C':
						res.push_back('A');
						lastBase='A';
						break;
					case 'G':
						res.push_back('C');
						lastBase='C';
						break;
					case 'T':
						res.push_back('G');
						lastBase='G';
						break;
				}
				break;
		}
	}
	return res;
}


char automateDecode(char currBase,char prevBase,bool rep)
{
	if(!rep)
	{
		switch(currBase)
		{
			case 'A':
				switch(prevBase)
				{
					case 'A':
						return ('0');
						break;
					case 'G':
						return ('1');
						break;
					case 'C':
						return ('2');
						break;
					default:
						return('X');
						break;
				}
				break;
			case 'C':
				switch(prevBase)
				{
					case 'G':
						return ('2');
						break;
					case 'C':
						return ('0');
						break;
					case 'T':
						return ('1');
						break;
					default:
						return('X');
						break;
				}
				break;
			case 'G':
				switch(prevBase)
				{
					case 'A':
						return ('1');
						break;
					case 'T':
						return ('2');
						break;
					case 'G':
						return ('0');
						break;
					default:
						return('X');
						break;
				}
				break;
			case 'T':
				switch(prevBase)
				{
					case 'C':
						return ('1');
						break;
					case 'A':
						return ('2');
						break;
					case 'T':
						return ('0');
						break;
					default:
						return('X');
						break;
				}
				break;
		}
	}
	else
	{
		switch(currBase)
		{
            case 'A':
				switch(prevBase)
				{
					case 'T':
						return ('0');
						break;
					case 'G':
						return ('1');
						break;
					case 'C':
						return ('2');
						break;
					default:
						return('X');
						break;
				}
				break;
			case 'C':
				switch(prevBase)
				{
					case 'G':
						return ('2');
						break;
					case 'A':
						return ('0');
						break;
					case 'T':
						return ('1');
						break;
					default:
						return('X');
						break;
				}
				break;
			case 'G':
				switch(prevBase)
				{
					case 'A':
						return ('1');
						break;
					case 'T':
						return ('2');
						break;
					case 'C':
						return ('0');
						break;
					default:
						return('X');
						break;
				}
				break;
			case 'T':
				switch(prevBase)
				{
					case 'C':
						return ('1');
						break;
					case 'A':
						return ('2');
						break;
					case 'G':
						return ('0');
						break;
					default:
						return('X');
						break;
				}
				break;
		}
	}
	return 'X';
}


std::string decode(std::string s)
{
	bool HP=false; 
	int n=s.length();

	std::string res="";

	for(int i=n-1;i>=0;i--)
	{
		if(i>=3)
		{
			if((s[i-1]==s[i-2])&&(s[i-2]==s[i-3]))
				HP=true;

			res.insert(0,1,automateDecode(s[i],s[i-1],HP));
		}
		else //consider we got "AAA" before
		{
			if(i==0)
			{
				res.insert(0,1,automateDecode(s[i],'A',true));
			}
			else
			{
				res.insert(0,1,automateDecode(s[i],s[i-1],false));
			}
		}
		HP=false;
	}
	return res;
}


std::string apply_encoding(std::string sequence)
{
	std::string res="";
	std::string res2="";
	std::string res3="";

	baseToQuat(sequence,res);
	quatToBin(res,res2);
	binToTrit(res2,res3);

	std::string encoding_result = encode(res3);

	return encoding_result;
}


std::string apply_decoding(std::string sequence)
{
	std::string res="";
	std::string res2="";
	std::string res3="";

	std::string dec = decode(sequence);

	tritToBin(dec,res);
	binToQuat(res,res2);
	quatToBase(res2,res3);

	return res3;
}


void avoid_HP_on_file(std::string code, std::string input_file, std::string output_file)
{
	std::string line;
	std::ifstream input_stream (input_file);
	std::ofstream output_stream (output_file);

	if (input_stream.is_open() and output_stream.is_open())
	{
		if(code[0]=='1') //encode
		{
			while( getline(input_stream, line) )
			{
				std::string seq_name = line;
				getline(input_stream, line); //get a new line to skip the sequence name

				std::string encoding_result = apply_encoding(line);

				//write result to output
				output_stream << seq_name << "\n";
				output_stream << encoding_result << "\n";
			}
		}
		else //decode
		{
			while( getline(input_stream, line) )
			{
				std::string seq_name = line;
				getline(input_stream, line); //get a new line to skip the sequence name

				std::string decoding_result = apply_decoding(line);

				//write result to output
				output_stream << seq_name << "\n";
				output_stream << decoding_result << "\n";
			}
		}
		input_stream.close();
		output_stream.close();
	}
}


int main(int argc, char *argv[])
{
	if (argc == 3) // apply homopolymere deletion on an input string an print the result
	{
		int HPMax=3; // A version with a dynamic maximum HP length is on debugging...

		if(argv[1][0]=='1') //encode
		{
			std::string encoding_result = apply_encoding(argv[2]);
			std::cout << encoding_result << "\n";

		}
		else //decoder
		{
			std::string decoding_result = apply_decoding(argv[2]);
			std::cout << decoding_result << "\n";
		}
	}

	if (argc == 4) // apply homopolymere deletion on an input fasta file and save the result in an output fasta file
	{
		avoid_HP_on_file(argv[1], argv[2], argv[3]);
	}

	if(argc!=3 and argc!=4) //incorrect number of arguments
	{
		printf("usage : noLongHP3 code input_string\n\tor noLongHP3 code input_file output_file\n\tcode=1 for encoding; code=2 for decoding\n");
		return 1;
	}
}

