#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project: DnarXiv, funded by CominLabs				    #
#---------------------------------------------------------------------------#
# 			DNA Data Storage SIMULATOR			    #
#      -Synthesis, Storage, preparation, nanopore sequencing, basecalling - #
#									    #
#---------------------------------------------------------------------------#

function randomBase()
	base=rand((0,1,2,3))

	if(base==0)
		return "A"
	elseif(base==1)
		return "C"
	elseif(base==2)
		return "G"
	else
		return "T"
	end
end

function insertB(seqTmp,rangeLenIns,lenIns)
	r3=rand()
	len=1;

	#length of the insertion
	for j=1:lenIns

		if (r3<=rangeLenIns[j])
			len=j;
			break;
		end

	end

	for j=1:len #insert after current pos
		base=randomBase() #pickup randomly a base to insert
		push!(seqTmp,base)
	end

end


function deleteB(seqTmp,rangeLenDel,lenDel)
	len=1; #length of del is 1 (bursts are taken into account in another way)
	#Do nothing => deletion
end

function substB(seqTmp,rangeTransProb,currBase)
	r=rand()
	base=""
	if(currBase=='A')	
		for j=1:length(rangeTransProb[1])
			if (r<=rangeTransProb[1][j])
				base=split(subList[1,j],"2")[2] 
				break;
			end
		end
	elseif(currBase=='C')
		for j=1:length(rangeTransProb[2])
			if (r<=rangeTransProb[2][j])
				base=split(subList[2,j],"2")[2] 
				break;
			end
		end
	elseif(currBase=='G')
		for j=1:length(rangeTransProb[3])
			if (r<=rangeTransProb[3][j])
				base=split(subList[3,j],"2")[2] 
				break;
			end
		end
	elseif(currBase=='T')
		for j=1:length(rangeTransProb[4])
			if (r<=rangeTransProb[4][j])
				base=split(subList[4,j],"2")[2]
				break;
			end
		end
	end
	push!(seqTmp,base)
end


function matchB(seqTmp,currBase)
	push!(seqTmp,currBase)
end




#############################
#	Main()		    #

#############################
using DelimitedFiles



if(isempty(ARGS) || length(ARGS) < 4)
	println("error: not Enough arguments !")
	exit(1)
end


k=parse(Int64,ARGS[4])


tmp=readdlm("./probEdit/BegErrByPosAvg.txt");
BegProbEdit=Float64.(tmp[1]) 
tmp=readdlm("./probEdit/BegInsByPosAvg.txt")
BegProbIns=Float64.(tmp[1])
tmp=readdlm("./probEdit/BegDelByPosAvg.txt")
BegProbDel=Float64.(tmp[1])
tmp=readdlm("./probEdit/BegMisAvg.txt")
BegProbSubst=Float64.(tmp[1])


tmp=readdlm("./probEdit/AErrAvg.txt");
MidProbEditA=Float64.(tmp[1])
tmp=readdlm("./probEdit/AInsAvg.txt")
MidProbInsA=Float64.(tmp[1])
tmp=readdlm("./probEdit/ADelAvg.txt")
MidProbDelA=Float64.(tmp[1])
tmp=readdlm("./probEdit/AMisAvg.txt")
MidProbSubstA=Float64.(tmp[1])


tmp=readdlm("./probEdit/CErrAvg.txt");
MidProbEditC=Float64.(tmp[1]) 
tmp=readdlm("./probEdit/CInsAvg.txt")
MidProbInsC=Float64.(tmp[1])
tmp=readdlm("./probEdit/CDelAvg.txt")
MidProbDelC=Float64.(tmp[1])
tmp=readdlm("./probEdit/CMisAvg.txt")
MidProbSubstC=Float64.(tmp[1])


tmp=readdlm("./probEdit/GErrAvg.txt");
MidProbEditG=Float64.(tmp[1]) 
tmp=readdlm("./probEdit/GInsAvg.txt")
MidProbInsG=Float64.(tmp[1])
tmp=readdlm("./probEdit/GDelAvg.txt")
MidProbDelG=Float64.(tmp[1])
tmp=readdlm("./probEdit/GMisAvg.txt")
MidProbSubstG=Float64.(tmp[1])


tmp=readdlm("./probEdit/TErrAvg.txt");
MidProbEditT=Float64.(tmp[1]) 
tmp=readdlm("./probEdit/TInsAvg.txt")
MidProbInsT=Float64.(tmp[1])
tmp=readdlm("./probEdit/TDelAvg.txt")
MidProbDelT=Float64.(tmp[1])
tmp=readdlm("./probEdit/TMisAvg.txt")
MidProbSubstT=Float64.(tmp[1])

subList=["A2C" "A2G" "A2T";
 "C2A" "C2G" "C2T";
 "G2A" "G2C" "G2T";
 "T2A" "T2C" "T2G"]


Yi=["M" "D" "I" "S"]


#write unknown k-mers (doesn't exist on the errorProfile) to track them
logUnknownKmers=open("./unknownKmers_log.txt","a") #w:truncate, a :append    


transProb=[]
for i=1:length(subList[:,1])
tmpProb=zeros(Float64,0)
for j=1:length(subList[1,:])
	avgTmp=Float64.(readdlm("./probEdit/$(subList[i,j])_Avg.txt")[1]);
	push!(tmpProb,avgTmp);
end
push!(transProb,tmpProb)
end

for i=1:length(subList[:,1])
transProb[i]=transProb[i]/sum(transProb[i])
end

rangeTransProb=[] #2D table to allow easy mapping to subList

for i=1:length(subList[:,1])
	tmpRangeLine=[]
	for j=1:length(subList[1,:])
		if(j>1)
			push!(tmpRangeLine,tmpRangeLine[j-1]+transProb[i][j]);
		else
			push!(tmpRangeLine,transProb[i][j]);
		end
	end
	push!(rangeTransProb,tmpRangeLine)
end




#Probability to observe Yi on kmer i, knowing that Yi-1 occured
#index: 1-> prev Match, 2-> prev Del, 3-> prev Ins, 4-> prev Subst,
EditYiKmerprevYi=[]
mapKmerPrevYi=[]


#Yi-1= [Match[Yi=I,D,S,E,M],Del[Yi=I,D,S,E,M],Ins[Yi=I,D,S,E,M],Sub[Yi=I,D,S,E,M]]
for e1=1:4
	if(isfile("./probEdit/KmerYi_prevYi$(Yi[e1])_RatesAvg.txt") )
		#kmer Ins Del Subst Err Match
		push!(EditYiKmerprevYi,readdlm("./probEdit/KmerYi_prevYi$(Yi[e1])_RatesAvg.txt"));
		nKmer=length(EditYiKmerprevYi[e1][:,1]) #nbr of entries

		#compute avg probabilities (useful to use them when an unknown kmer is read)
		#add an extra index containing average probabilities that we can use when k-mer unknown
		avgIns=sum(EditYiKmerprevYi[e1][:,2])/nKmer
		avgDel=sum(EditYiKmerprevYi[e1][:,3])/nKmer
		avgSub=sum(EditYiKmerprevYi[e1][:,4])/nKmer
		avgErr=sum(EditYiKmerprevYi[e1][:,5])/nKmer
		avgMatch=sum(EditYiKmerprevYi[e1][:,6])/nKmer
		EditYiKmerprevYi[e1]=[EditYiKmerprevYi[e1] ; "" avgIns avgDel avgSub avgErr avgMatch];

		#create a dictionar (kmer,index) to reach the index (in KmerProb)containing probabilities (edit Rates table)
		push!(mapKmerPrevYi,Dict())
		nKmer=nKmer+1 #to take in account last added row (avgvalues)
		for i=1:nKmer
			mapKmerPrevYi[e1][EditYiKmerprevYi[e1][i,1]]=i
		end

	else
		push!(mapKmerPrevYi,Dict())
		push!(EditYiKmerprevYi,[]);
	end

end


#index: 1-> prev Match, 2-> prev Del, 3-> prev Ins, 4-> prev Subst,

kmerInsLenProbPrevYi=[]
mapKmerInsLenPrevYi=[]
#Yi-1= [Match[Yi=I,D,S,E,M],Del[Yi=I,D,S,E,M],Ins[Yi=I,D,S,E,M],Sub[Yi=I,D,S,E,M]]
for e1=1:4
	if(isfile("./probEdit/KmerInsLen_prevYi$(Yi[e1])_RatesAvg2.txt") )
		kmerInsLenProbTmp=readdlm("./probEdit/KmerInsLen_prevYi$(Yi[e1])_RatesAvg2.txt");

			#create a dictionar (kmer,index) to reach the index (in KmerProb)containing probabilities (edit Rates table)

		push!(mapKmerInsLenPrevYi,Dict())

		push!(kmerInsLenProbPrevYi,[]) #will contains prob of ins Length by column (each column => length) instead of string with ',' char
		nKmer=length(kmerInsLenProbTmp[:,1]) #nbr of entries
		for i=1:nKmer
			mapKmerInsLenPrevYi[e1][kmerInsLenProbTmp[i,1]]=i
			tmpLine=parse.(Float64,split(join(kmerInsLenProbTmp[i,2:end]),","))
			#do accumulation to divide ranges (1ins (0->0.20 ) , 2ins (0.20->0.30),...)
			for j=1:length(tmpLine)
				if(j>1)
						tmpLine[j]+=tmpLine[j-1]
				end
			end
			push!(kmerInsLenProbPrevYi[e1],tmpLine)
		end
		kmerInsLenProbTmp=Nothing; # remove tmp

		lenMax=length(kmerInsLenProbPrevYi[e1][1]); #number of column
		avgInsLen=[];
		#Avg value to get an insertion of length c
		for c=1:lenMax
			avg=0
			for k=1:nKmer
				avg+=kmerInsLenProbPrevYi[e1][k][c]
			end
			avg=avg/nKmer
			push!(avgInsLen,avg);
		end

		push!(kmerInsLenProbPrevYi[e1],Float64.(avgInsLen))
		#kmerInsLenProbPrevYi[e1]=[kmerInsLenProbPrevYi[e1];];
		#add an extra index to reach average probabilities that we can use when k-mer unknown
		mapKmerInsLenPrevYi[e1][""]=nKmer+1


	else
		push!(mapKmerInsLenPrevYi,Dict())
		push!(kmerInsLenProbPrevYi,[])
	end
end



kmerInsLenProbTmp=readdlm("./probEdit/KmerInsLenRatesAvg2.txt");
#kmer probInsLen=1 probInsLen=2 probInsLen=3
#create a dictionar (kmer,index) to reach the index (in kmerInsLenProb)containing probabilities of insertion length
mapInsLenKmer=Dict()
kmerInsLenProb=[] #will contains prob of ins Length by column (each column => length) instead of string with ',' char
nKmer=length(kmerInsLenProbTmp[:,1]) #nbr of entries
for i=1:nKmer
mapInsLenKmer[kmerInsLenProbTmp[i,1]]=i
tmpLine=parse.(Float64,split(join(kmerInsLenProbTmp[i,2:end]),","))
#do accumulation to divide ranges (1ins (0->0.20 ) , 2ins (0.20->0.30),...)
for j=1:length(tmpLine)
	if(j>1)
			tmpLine[j]+=tmpLine[j-1]
	end
end
push!(kmerInsLenProb,tmpLine)
end
kmerInsLenProbTmp=Nothing # remove tmp

#=

kmerDelLenProbTmp=readdlm("./probEdit/KmerDelLenRatesAvg2.txt");
#kmer probDelLen=1 probDelLen=2 probDelLen=3
#create a dictionar (kmer,index) to reach the index (in kmerDelLenProb)containing probabilities of insertion length
mapDelLenKmer=Dict()
kmerDelLenProb=[] #will contains prob of ins Length by column (each column => length) instead of string with ',' char
nKmer=length(kmerDelLenProbTmp[:,1]) #nbr of entries
for i=1:nKmer
mapDelLenKmer[kmerDelLenProbTmp[i,1]]=i
tmpLine=parse.(Float64,split(join(kmerDelLenProbTmp[i,2:end]),","))
#do accumulation to divide ranges (1ins (0->0.20 ) , 2ins (0.20->0.30),...)
for j=1:length(tmpLine)
	if(j>1)
			tmpLine[j]+=tmpLine[j-1]
	end
end
push!(kmerDelLenProb,tmpLine)
end
kmerDelLenProbTmp=Nothing # remove tmp
=#

#probDel of current k-mer knowing that a delete occurs previously (Yi(kmeri) depend on Y-1)
DelProb_YiprevDel=readdlm("./probEdit/KmerDel_delPrevRates.txt");
#kmer Occ Ins Del Subst Err Match
#create a dictionar (kmer,index) to reach the index (in KmerProb)containing probabilities (edit Rates table)
mapDelProb_YiprevDel=Dict()
nKmer=length(DelProb_YiprevDel[:,1]) #nbr of entries
for i=1:nKmer
mapDelProb_YiprevDel[DelProb_YiprevDel[i,1]]=DelProb_YiprevDel[i,2]
end



tmp=readdlm("./probEdit/EndErrByPosAvg.txt");
EndProbEdit=Float64.(tmp[1]) #majority vote Tab)
tmp=readdlm("./probEdit/EndInsByPosAvg.txt")
EndProbIns=Float64.(tmp[1])
tmp=readdlm("./probEdit/EndDelByPosAvg.txt")
EndProbDel=Float64.(tmp[1])
tmp=readdlm("./probEdit/EndMisAvg.txt")
EndProbSubst=Float64.(tmp[1])




tmp=readdlm("./probEdit/insLenBegRates.txt")[1,:]; #as a vector
pop!(tmp); #remove last blank
probInsLenBeg=Float64.(tmp) #majority vote Tab)


tmp=readdlm("./probEdit/AinsLenMidRates.txt")[1,:];
pop!(tmp); #remove last blank
probInsLenMidA=Float64.(tmp) #majority vote Tab)


tmp=readdlm("./probEdit/CinsLenMidRates.txt")[1,:];
pop!(tmp); #remove last blank
probInsLenMidC=Float64.(tmp) #majority vote Tab)

tmp=readdlm("./probEdit/GinsLenMidRates.txt")[1,:];
pop!(tmp); #remove last blank
probInsLenMidG=Float64.(tmp) #majority vote Tab)

tmp=readdlm("./probEdit/TinsLenMidRates.txt")[1,:];
pop!(tmp); #remove last blank
probInsLenMidT=Float64.(tmp) #majority vote Tab)



tmp=readdlm("./probEdit/insLenEndRates.txt")[1,:];
pop!(tmp); #remove last blank
probInsLenEnd=Float64.(tmp) #majority vote Tab)


nbrSim=parse(Int64,ARGS[1])
#nbrSim=3
probDelLen=0 # not USED!!!

#seq=split(ARGS[2],"")
seq=Char.(read(ARGS[2]))
tmpLen=length(seq)
if(seq[tmpLen]!='A' && seq[tmpLen]!='C' && seq[tmpLen]!='G' && seq[tmpLen]!='T')
	pop!(seq) #remove last element (\n or '' ,...)
end



	simSeq=[]

	#divide space (0->1, depending on their rates) to pickup the type of edition (3 types)
	#beg
	sumBeg=BegProbIns+BegProbDel+BegProbSubst
	rangeInsMaxBeg=BegProbIns/sumBeg;
	rangeDelMaxBeg=rangeInsMaxBeg+(BegProbDel/sumBeg);
	rangeSubstMaxBeg=rangeDelMaxBeg+(BegProbSubst/sumBeg);
	#mid
	sumMidA=MidProbDelA+MidProbSubstA
	rangeDelMaxMidA=(MidProbDelA/sumMidA);
	rangeSubstMaxMidA=rangeDelMaxMidA+(MidProbSubstA/sumMidA);

	sumMidC=MidProbDelC+MidProbSubstC
	rangeDelMaxMidC=(MidProbDelC/sumMidC);
	rangeSubstMaxMidC=rangeDelMaxMidC+(MidProbSubstC/sumMidC);

	sumMidG=MidProbDelG+MidProbSubstG
	rangeDelMaxMidG=(MidProbDelG/sumMidG);
	rangeSubstMaxMidG=rangeDelMaxMidG+(MidProbSubstG/sumMidG);


	sumMidT=MidProbDelT+MidProbSubstT
	rangeDelMaxMidT=(MidProbDelT/sumMidT);
	rangeSubstMaxMidT=rangeDelMaxMidT+(MidProbSubstT/sumMidT);

	#endrangeLenInsBeg
	sumEnd=EndProbIns+EndProbDel+EndProbSubst
	rangeInsMaxEnd=EndProbIns/sumEnd;
	rangeDelMaxEnd=rangeInsMaxEnd+(EndProbDel/sumEnd);
	rangeSubstMaxEnd=rangeDelMaxEnd+(EndProbSubst/sumEnd);


	#divide  space to pickup the length of the edition
	#Beg
	lenInsBeg=length(probInsLenBeg);
	rangeLenInsBeg=[]
	for j=1:lenInsBeg	 
		if(j>1)
			push!(rangeLenInsBeg,rangeLenInsBeg[j-1]+probInsLenBeg[j])
		else
			push!(rangeLenInsBeg,probInsLenBeg[1])
		end
	end

	#mid
	lenInsMidA=length(probInsLenMidA);
	rangeLenInsMidA=[]
	for j=1:lenInsMidA	 
		if(j>1)
			push!(rangeLenInsMidA,rangeLenInsMidA[j-1]+probInsLenMidA[j])
		else
			push!(rangeLenInsMidA,probInsLenMidA[1])
		end
	end

	lenInsMidC=length(probInsLenMidC);
	rangeLenInsMidC=[]
	for j=1:lenInsMidC	 	
		if(j>1)
			push!(rangeLenInsMidC,rangeLenInsMidC[j-1]+probInsLenMidC[j])
		else
			push!(rangeLenInsMidC,probInsLenMidC[1])
		end
	end


	lenInsMidG=length(probInsLenMidG);
	rangeLenInsMidG=[]
	for j=1:lenInsMidG	 
		if(j>1)
			push!(rangeLenInsMidG,rangeLenInsMidG[j-1]+probInsLenMidG[j])
		else
			push!(rangeLenInsMidG,probInsLenMidG[1])
		end
	end

	lenInsMidT=length(probInsLenMidT);
	rangeLenInsMidT=[]
	for j=1:lenInsMidT	
		if(j>1)
			push!(rangeLenInsMidT,rangeLenInsMidT[j-1]+probInsLenMidT[j])
		else
			push!(rangeLenInsMidT,probInsLenMidT[1])
		end
	end


	#end
	lenInsEnd=length(probInsLenEnd);
	rangeLenInsEnd=[]
	for j=1:lenInsEnd	
		if(j>1)
			push!(rangeLenInsEnd,rangeLenInsEnd[j-1]+probInsLenEnd[j])
		else
			push!(rangeLenInsEnd,probInsLenEnd[1])
		end
	end


lenDel=length(probDelLen);
rangeLenDel=[]
for j=1:lenDel 
	if(j>1)
		push!(rangeLenDel,rangeLenDel[j-1]+probDelLen[j])
	else
		push!(rangeLenDel,probDelLen[1])
	end
end



iRef=[1]
startKmer=[1]

for iSeq=1:nbrSim
	print("\rSim=",iSeq)
	nRef=length(seq)


	iRef[1]=1	#used to go throug the reference seq
	kmer=""
	prevKmer=""   #used to know which kmer was before (to process ) NOT USED YET
	prevEdit=""   #used to know which edition was previously observed [1:M,2:D,3:I,4:S]

	if(k>1) #first base is affected by beg probabilities
		startKmer[1]=1 #first Kmer pos
	else
		startKmer[1]=2 #first Kmer pos
		#prevEdit="M" #As first Kmer will start at pos k first prevEdit will be at k pos
	end



	push!(simSeq,[])


	while iRef[1] <= nRef

		currBase=seq[iRef[1]]
		if(iRef[1]>1) #when 1<iRef<k => use posBypos (length(kmer)<k =>posBypos done automatically )

			if(iRef[1]<=nRef-1) 

				r1=rand()

				kmer=join(seq[startKmer[1]:iRef[1]]) 



				if(iRef[1]>=k ) #we have at least k base on the nanopore
					indexP=0

					if(get(mapKmerPrevYi[prevEdit],kmer,0)>0) #kmer entry exist
						indexP=mapKmerPrevYi[prevEdit][kmer] #mapKmer=["ACGA"]
					else #unknown kmer -> use average values
						indexP=mapKmerPrevYi[prevEdit][""]

						# (BACKtrack unknown k-mers)
						if(iSeq==1 && iRef[1]>=k ) #save them only one time
							write(logUnknownKmers,kmer,"\n")
						end

					end



					#kmer INS	DEL	SUBST	ERR	MATCH

					probInsK=EditYiKmerprevYi[prevEdit][indexP,2]
					probDelK=EditYiKmerprevYi[prevEdit][indexP,3]
					probSubstK=EditYiKmerprevYi[prevEdit][indexP,4]
					probMatchK=EditYiKmerprevYi[prevEdit][indexP,6]
					probEditK=probDelK+probSubstK #Insertions can only appear after susbt or Match (otherwise prob not considered) 

					if(r1<=probEditK) #edit the kmer


							rangeDelMaxK=probDelK/probEditK
							rangeSubstMaxK=rangeDelMaxK+(probSubstK/probEditK)


							r2=rand()
				
							if(r2<=rangeDelMaxK) #Deletion
										
								deleteB(simSeq[iSeq],rangeLenDel,lenDel)
								prevEdit=2 #DEL
							elseif(r2<=rangeSubstMaxK) #Substitution

								substB(simSeq[iSeq],rangeTransProb,currBase)
								prevEdit=4 #Subs

								r3=rand()
								if (r3<=probInsK) #INS :In This version insertion appears after Match or Subst (=>extra char between it and next kmer)
										indexPLen=0
										if(get(mapKmerInsLenPrevYi[prevEdit],kmer,0)>0) #entry exist
											indexPLen=mapKmerInsLenPrevYi[prevEdit][kmer] 
										else  #entry doesn't exist -> use avg values (of all known kmers)
											indexPLen=mapKmerInsLenPrevYi[prevEdit][""]
										end

										rangeLenInsK=kmerInsLenProbPrevYi[prevEdit][indexPLen]
										lenInsK=length(rangeLenInsK)
										insertB(simSeq[iSeq],rangeLenInsK,lenInsK)

										prevEdit=3 #INS
								end
							end


					else
						matchB(simSeq[iSeq],currBase)
						prevEdit=1 #Match

						r3=rand()
						if (r3<=probInsK) #INS:#In This version insertion appears after Match or Subst 
								indexPLen=0
								if(get(mapKmerInsLenPrevYi[prevEdit],kmer,0)>0) #entry exist
									indexPLen=mapKmerInsLenPrevYi[prevEdit][kmer] #mapKmer=["ACGA"]
								else  #entry doesn't exist -> use avg values (of all known kmers)
									indexPLen=mapKmerInsLenPrevYi[prevEdit][""]
								end

								rangeLenInsK=kmerInsLenProbPrevYi[prevEdit][indexPLen]
								lenInsK=length(rangeLenInsK)
								insertB(simSeq[iSeq],rangeLenInsK,lenInsK)

								prevEdit=3 #INS
						end

					end
				else #number of bases in the nanopore are less than k -> P(YI|XI) 

					if(seq[iRef[1]]=="A")
						if(r1<=MidProbEditA) #edit the nt

							r2=rand()

							if (r2<=rangeDelMaxMidA)
								deleteB(simSeq[iSeq],rangeLenDel,lenDel)
								prevEdit=2 #DEL
							elseif (r2<=rangeSubstMaxMidA) #substitution
								substB(simSeq[iSeq],rangeTransProb,currBase)
								prevEdit=4 #SUBS

								r3=rand()
								if (r3<=MidProbInsA) #insertion
										insertB(simSeq[iSeq],rangeLenInsMidA,lenInsMidA)
										prevEdit=3 #INS
								end

							end
						else
							matchB(simSeq[iSeq],currBase)
							prevEdit=1 #Match

							r3=rand()
							if (r3<=MidProbInsA) #insertion
									insertB(simSeq[iSeq],rangeLenInsMidA,lenInsMidA)
									prevEdit=3 #INS
							end

						end
					elseif(seq[iRef[1]]=="C")
						if(r1<=MidProbEditC) #edit the nt

							r2=rand()

							if (r2<=rangeDelMaxMidC)
								deleteB(simSeq[iSeq],rangeLenDel,lenDel)
								prevEdit=2 #DEL
							elseif (r2<=rangeSubstMaxMidC) #substitution
								substB(simSeq[iSeq],rangeTransProb,currBase)
								prevEdit=4 #SUBS

								r3=rand()
								if (r3<=MidProbInsC) #insertion
										insertB(simSeq[iSeq],rangeLenInsMidC,lenInsMidC)
										prevEdit=3 #INS
								end

							end
						else
							matchB(simSeq[iSeq],currBase)
							prevEdit=1 #Match

							r3=rand()
							if (r3<=MidProbInsC) #insertion
									insertB(simSeq[iSeq],rangeLenInsMidC,lenInsMidC)
									prevEdit=3 #INS
							end

						end
					elseif(seq[iRef[1]]=="G")
						if(r1<=MidProbEditG) #edit the nt

							r2=rand()

						
							if (r2<=rangeDelMaxMidG)
								deleteB(simSeq[iSeq],rangeLenDel,lenDel)
								prevEdit=2 #DEL
							elseif (r2<=rangeSubstMaxMidG) #substitution
								substB(simSeq[iSeq],rangeTransProb,currBase)
								prevEdit=4 #SUBS

								r3=rand()
								if (r3<=MidProbInsG) #insertion
										insertB(simSeq[iSeq],rangeLenInsMidG,lenInsMidG)
										prevEdit=3 #INS
								end

							end
						else
							matchB(simSeq[iSeq],currBase)
							prevEdit=1 #Match

							r3=rand()
							if (r3<=MidProbInsG) #insertion
									insertB(simSeq[iSeq],rangeLenInsMidG,lenInsMidG)
									prevEdit=3 #INS
							end

						end
					elseif(seq[iRef[1]]=="T")
						if(r1<=MidProbEditT) #edit the nt

							r2=rand()

						
							if (r2<=rangeDelMaxMidT)
								deleteB(simSeq[iSeq],rangeLenDel,lenDel)
								prevEdit=2 #DEL
							elseif (r2<=rangeSubstMaxMidT) #substitution
								substB(simSeq[iSeq],rangeTransProb,currBase)
								prevEdit=4 #SUBS

								r3=rand()
								if (r3<=MidProbInsT) #insertion
										insertB(simSeq[iSeq],rangeLenInsMidT,lenInsMidT)
										prevEdit=3 #INS
								end

							end
						else
							matchB(simSeq[iSeq],currBase)
							prevEdit=1 #Match

							r3=rand()
							if (r3<=MidProbInsT) #insertion
									insertB(simSeq[iSeq],rangeLenInsMidT,lenInsMidT)
									prevEdit=3 #INS
							end

						end
					end

				end
				if(iRef[1]>=k)
					prevKmer=kmer
					startKmer[1]+=1
				end
			else #i==n last position (end part)



				r1=rand()

				if(r1<=EndProbEdit) #edit the nt

					r2=rand()

					if (r2<=rangeInsMaxEnd) #insertion
						insertB(simSeq[iSeq],rangeLenInsEnd,lenInsEnd)
						prevEdit=3 #INS
					elseif (r2<=rangeDelMaxEnd) #deletion
						deleteB(simSeq[iSeq],rangeLenDel,lenDel)
						prevEdit=2 #DEL
					elseif (r2<=rangeSubstMaxEnd) #substitution
						substB(simSeq[iSeq],rangeTransProb,currBase)
						prevEdit=4 #SUBS
					end
				else
					matchB(simSeq[iSeq],currBase)
					prevEdit=1 #Match
				end
			end
		else #i==1 || i<k first position (Beg part)



			r1=rand()

			if(r1<=BegProbEdit) #edit the nt

				r2=rand()

				if (r2<=rangeInsMaxBeg) #insertion
					insertB(simSeq[iSeq],rangeLenInsBeg,lenInsBeg)
					prevEdit=3 #INS
				elseif (r2<=rangeDelMaxBeg) #deletion
					deleteB(simSeq[iSeq],rangeLenDel,lenDel)
					prevEdit=2 #DEL
				elseif (r2<=rangeSubstMaxBeg) #substitution
					substB(simSeq[iSeq],rangeTransProb,currBase)
					prevEdit=4 #SUBS
				end
			else
				matchB(simSeq[iSeq],currBase)
				prevEdit=1 #Match  ************
			end

		end



		iRef[1]+=1;
	end


end
println()

#write results in a file

outSim=ARGS[3]
outFile=open(outSim,"w") #w:truncate, a :append


#write them as a fastq file even if it isn't to avoid compatibility issues
for iSeq=1:nbrSim
	len=length(simSeq[iSeq])
	write(outFile,">simulation $iSeq \n")
	for j=1:len
			write(outFile,simSeq[iSeq][j])
	end
	write(outFile,"\n+\n")
	write(outFile,"@\n")
end
close(outFile)


close(logUnknownKmers) # track unknown k-mers
