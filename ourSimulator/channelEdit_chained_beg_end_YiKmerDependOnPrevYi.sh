#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#


########################################################
#  take probabilities & other inputs (seq & N) to simulate
#######################################################
#$1:reference sequence,$2: number of sequence to generate (simulate),$4:output folder



prevDir=$(pwd)

cd $(dirname $0)

	
	
	refFile=tmpRefFile.txt
	
	echo $1 > $refFile

	n=$2; #nbr of seq to simulate


	simOut=$3 #output fastqFile

	k=6

	if [ ! -z "$4" ]
	then
		k=$4

	fi
	
	rm -f ./unknownKmers_log.txt #remove unknown k-mers file tracker



	julia simSeq_beg_end_YiXi_transProb_Mem_YiKmerDependOnPrevYi_Release.jl  $n $refFile $simOut $k



cd $prevDir
