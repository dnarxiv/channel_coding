#---------------------------------------------------------------------------#
#	@Authors: Belaid Hamoum, Elsa Dupraz				    #
#	Project : DnarXiv, funded by CominLabs				    #
#	email   : belaid.hamoum@gmail.com				    #
#---------------------------------------------------------------------------#

#echo "-------------------------------------------"
#echo "|  Channel_decoder: LDPC BP DECODER       |"
#echo "-------------------------------------------"

	

	####
	#->INPUTS:
	#	-$1:selected consensus path
	#	-$2:expected Length (to use synchronization if necessary
	#	-$3:output_path 
	#	-$4:H_matrix path 
	#->OUTPUTS:
	#	-output_path : decoded sequence output
	#	-print: 1-> the decoded sequence is correct, 0-> the decoded sequence is incorrect
	###

prevDir=$(pwd)

cd $(dirname $0)

cd ./LDPC

	inPath=""
	outPath="" 
	refLen="" 
	H_Matrix=""

	#--
	#Check variables

	while [ "$1" != "" ]; do
	    case $1 in
		-i )
		    shift
		    inPath=$1
		;;
		-o )
		    shift
		    outPath=$1  
		;;            
		-l )    
		    shift
		    refLen=$1
		;;
		-H  )   
		    shift
		    H_Matrix=$1
		;;

	    esac
	    shift
	done


	if [ -f "$prevDir/$inPath" ]
	then
		inPath=$prevDir/$inPath
	elif [ -f "$inPath" ]
	then
		inPath=$inPath
	else
		echo "error: input path is incorrect"
		exit 1
	fi


	int='^[1-9]+[0-9]*$'
	if ! [[ $refLen =~ $int ]]
	then
	   echo "error: -l value  should be greater than 0"
	   exit 1
	fi

	if ! touch $prevDir/$outPath > /dev/null 2>&1 
	then
		if ! touch $outPath > /dev/null 2>&1 
		then
			echo "error:output path incorrect "
			exit 1
		else
			outPath=$outPath
		fi

	else
		outPath=$prevDir/$outPath
	fi



	if [ -f "$prevDir/$H_Matrix" ]
	then
		H_Matrix=$prevDir/$H_Matrix
	elif [ -f "$H_Matrix" ]
	then
		H_Matrix=$H_Matrix
	else
		echo "error: H matrix path is incorrect"
		exit 1
	fi

	#Check variables end
	#----


	y=$(sed -n -e 2p $inPath)
	yLen=$(echo $y | tr -d '[:blank:]' | wc -m)
	((yLen=$yLen-1)) #remove extra last char 


	#Do synchronization?
	if [ $yLen -lt "$refLen" ]
	then
		y=$(julia NB_LDPC_decoder_DNA_sync.jl $inPath $H_Matrix)	
		#overwrite previous selected consensus
		echo ">" > $inPath
		echo "$y" >> $inPath
	fi



	#Use BP decoder
	outQuatDNA=./tmp/
	tmpCons=$(sed -n -e 2p $inPath)
	echo $tmpCons > ./tmp/tmpCons.txt 

	#convert DNA to quaternary  
	awk -v out=$outQuatDNA -f ../utility/DnaToQuat.awk ./tmp/tmpCons.txt
	#USE ELSA DECODER (LDPC SOFT)
	cd ./BPDecoder/
			#use matlab with communications system toolbox (30 days free trial) -> uncomment two following lines
				#*~/matlab/installFiles/bin/matlab -nodesktop -nodisplay -nosplash -r \
				#*"DNA_BP_LDPC_decoder('../$outQuatDNA','$H_Matrix');exit;" >> /dev/null 

			#or octave with communications package (free) 
				octave --silent --eval "DNA_BP_LDPC_decoderOctave('../$outQuatDNA','$H_Matrix');exit;">>/dev/null
	cd ../
	#convert Quaternary to Dna
	awk -v out=$outQuatDNA -f ../utility/QuatToDna.awk $outQuatDNA/decodedSeq.txt

	decSeq=$(sed -n -e 1p $outQuatDNA/decDNASeq.txt) #Contains decoded sequence in "ACGT" format

	res=($(julia NB_LDPC_decoder_DNA.jl $outQuatDNA/decDNASeq.txt $H_Matrix))   

	cValid=${res[0]}
	decLen=${res[1]}

	#DISPLAY DECODED SEQUENCE
	echo "${decSeq:0:decLen}" > $outPath

	#cValid=1 -> correct , 0-> incorrect
	echo $cValid

	#*rm $inPath #remove previous selected consensus  #Remove previous consensus

cd ..

cd $prevDir
